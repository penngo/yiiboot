
<?php
use yii\bootstrap5\ActiveForm;
use yii\helpers\Url;

?>

<?php $this->beginBlock('header');  ?>
<!-- <head></head>中代码块 -->
<?php $this->endBlock(); ?>

<!-- Main content -->
<section v-cloak class="content">
    <div id="msg_info"></div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">数据列表</h3>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <button id="create_btn" @click="createClick()" type="button" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i>添&nbsp;&emsp;加</button>
<!--                            |-->
                            <button id="delete_btn" @click="deleteClick()" type="button" class="btn btn-xs btn-danger">批量删除</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div id="data_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <!-- row start search-->
                        <div class="row">
                            <div class="col-sm-12">
                                <?php ActiveForm::begin(['id' => 'search-form', 'method'=>'get', 'options' => ['class' => 'form-inline'], 'action'=>Url::toRoute('admin-user/index')]); ?>                                
                    <div class="form-group" style="margin: 5px;">
                        <label>{{modelLabel.id}}:</label>
                        <input type="text" class="form-control" id="query[id]" name="query[id]"  :value="query['id']">
                    </div>
                                <div class="form-group">
                                    <a @click="searchClick" class="btn btn-primary btn-sm" href="#"> <i class="glyphicon glyphicon-zoom-in icon-white"></i>搜索</a>
                                </div>
                                <?php ActiveForm::end(); ?>                            </div>
                        </div>
                        <!-- row end search -->

                        <!-- row start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="data_table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="data_table_info">
                                    <thead class="text-nowrap">
                                    <tr role="row">
                                        <th><input id="data_table_check" type="checkbox"></th>
                                        <th @click="orderClick('id')" :class="orderClass('id')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.id}}</th>
                                        <th @click="orderClick('uname')" :class="orderClass('uname')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.uname}}</th>
                                        <th @click="orderClick('real_name')" :class="orderClass('real_name')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.real_name}}</th>
                                        <th @click="orderClick('last_ip')" :class="orderClass('last_ip')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.last_ip}}</th>
                                        <th @click="orderClick('last_time')" :class="orderClass('last_time')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.last_time}}</th>
                                        <th @click="orderClick('is_online')" :class="orderClass('is_online')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.is_online}}</th>
                                        <th @click="orderClick('status')" :class="orderClass('status')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.status}}</th>
                                        <th @click="orderClick('update_user')" :class="orderClass('update_user')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.update_user}}</th>
                                        <th @click="orderClick('update_date')" :class="orderClass('update_date')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.update_date}}</th>
                                        <th tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <template v-for="m in models">
                                        <tr :id="'rowid_' + m.id">
                                            <td><label><input type="checkbox" :value="m.id"></label></td>
                                            <td>{{m.id}}</td>
                                            <td>{{m.uname}}</td>
                                            <td>{{m.real_name}}</td>
                                            <td>{{m.last_ip}}</td>
                                            <td>{{m.last_time}}</td>
                                            <td>{{m.is_online == 'n' ? '是' : '否'}}</td>
                                            <td>{{loginStatus(m.status)}}</td>
                                            <td>{{m.update_user}}</td>
                                            <td>{{m.update_date}}</td>
                                            <td class="center">
                                                <a id="edit_btn" @click="editClick(m.id)" class="btn btn-primary btn-sm" href="#" title="修改"> <i class="glyphicon glyphicon-edit icon-white"></i></a>
                                                <a id="delete_btn" @click="deleteClick(m.id)" class="btn btn-danger btn-sm" href="#" title="删除"> <i class="glyphicon glyphicon-trash icon-white"></i></a>
                                            </td>
                                        </tr>
                                    </template>
                                    </tbody>
                                    <!-- <tfoot></tfoot> -->
                                </table>
                            </div>
                        </div>
                        <!-- row end -->

                        <!-- row start -->
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="data_table_info" role="status" aria-live="polite">
                                    <div class="infos">
                                        从 {{startNum}}            		到  {{endNum}}            		 共 {{totalNum}} 条记录</div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="data_table_paginate">
                                    <span v-if="pages != ''" v-html="pages"></span>
                                </div>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<div class="modal fade" id="edit_dialog" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">编辑</h4>
            </div>
            <div class="modal-body">
            <?php $form = ActiveForm::begin(["id" => "edit-form", "class"=>"form-horizontal", "action"=>Url::toRoute("admin-user/save")]); ?>                
            <input type="hidden" class="form-control" id="id" name="id" v-model="model.id" />

            <div id="uname_div" class="form-group">
                <label for="uname" class="col-sm-2 control-label">{{modelLabel.uname}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="uname" name="SystemUser[uname]" placeholder="必填" v-model="model.uname" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="real_name_div" class="form-group">
                <label for="real_name" class="col-sm-2 control-label">{{modelLabel.real_name}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="real_name" name="SystemUser[real_name]" placeholder="" v-model="model.real_name" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="last_ip_div" class="form-group">
                <label for="last_ip" class="col-sm-2 control-label">{{modelLabel.last_ip}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="last_ip" name="SystemUser[last_ip]" placeholder="" v-model="model.last_ip" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="last_time_div" class="form-group">
                <label for="last_time" class="col-sm-2 control-label">{{modelLabel.last_time}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="last_time" name="SystemUser[last_time]" placeholder="" v-model="model.last_time" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="is_online_div" class="form-group">
                <label for="is_online" class="col-sm-2 control-label">{{modelLabel.is_online}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="is_online" name="SystemUser[is_online]" placeholder="" v-model="model.is_online" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="status_div" class="form-group">
                <label for="status" class="col-sm-2 control-label">{{modelLabel.status}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="status" name="SystemUser[status]" placeholder="必填" v-model="model.status" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="des_div" class="form-group">
                <label for="des" class="col-sm-2 control-label">{{modelLabel.des}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="des" name="SystemUser[des]" placeholder="" v-model="model.des" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="create_user_div" class="form-group">
                <label for="create_user" class="col-sm-2 control-label">{{modelLabel.create_user}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="create_user" name="SystemUser[create_user]" placeholder="" v-model="model.create_user" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="create_date_div" class="form-group">
                <label for="create_date" class="col-sm-2 control-label">{{modelLabel.create_date}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="create_date" name="SystemUser[create_date]" placeholder="" v-model="model.create_date" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="update_user_div" class="form-group">
                <label for="update_user" class="col-sm-2 control-label">{{modelLabel.update_user}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="update_user" name="SystemUser[update_user]" placeholder="" v-model="model.update_user" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="update_date_div" class="form-group">
                <label for="update_date" class="col-sm-2 control-label">{{modelLabel.update_date}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="update_date" name="SystemUser[update_date]" placeholder="" v-model="model.update_date" />
                </div>
                <div class="clearfix"></div>
            </div>
            <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">关闭</a>
                <a @click="editSaveClick" id="edit_dialog_ok" href="#" class="btn btn-primary">确定</a>
            </div>
        </div>
    </div>
</div>
<?php $this->beginBlock('footer');  ?>
<!-- <body></body>后代码块 -->
<script>
    var app = new Vue({
        el: '.content-wrapper',  //  .content
        data: {
            modelLabel:<?=$modelLabel?>,
            model: {},
            models:<?=$models?>,
            pages:'<?=$pages?>',
            startNum:<?=$startNum?>,
            endNum:<?=$endNum?>,
            totalNum:<?=$totalNum?>,
            query:!!<?=$query?> ? <?=$query?> : {},
        },
        methods:{
            refreshPage:function(){
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.models = [];
                        app.models = data.data.models;
                        app.pages = data.data.pages;
                        app.startNum = data.data.startNum;
                        app.endNum = data.data.endNum;
                        app.totalNum = data.data.totalNum;
                    }
                });
            },
            createClick:function(){
                app.model = {};
                $('#edit_dialog').modal('show');
            },
            searchClick:function(){
                $('#search-form').submit();
            },
            orderClick:function(field){
                var url = window.location.search;
                var optemp = field + " desc";
                if(url.indexOf('orderby') != -1){
                    url = url.replace(/orderby=([^&?]*)/ig,  function($0, $1){
                        var optemp = field + " desc";
                        optemp = decodeURI($1) != optemp ? optemp : field + " asc";
                        return "orderby=" + optemp;
                    });
                }
                else{
                    if(url.indexOf('?') != -1){
                        url = url + "&orderby=" + encodeURI(optemp);
                    }
                    else{
                        url = url + "?orderby=" + encodeURI(optemp);
                    }
                }
                window.location.href=url;
            },
            editClick:function(id){
                $.ajax({
                    type: "GET",
                    url: "<?=Url::toRoute('admin-user/view')?>",
                    data: {"id":id},
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.model = data;
                        $("#update_date").attr({readonly:false,disabled:false});
                        $('#edit_dialog').modal('show');
                    }
                });
            },
            deleteClick:function(id){
                var ids = [];
                if(!!id == true){
                    ids[0] = id;
                }
                else{
                    var checkboxs = $('#data_table :checked');
                    if(checkboxs.size() > 0){
                        var c = 0;
                        for(i = 0; i < checkboxs.size(); i++){
                            var id = checkboxs.eq(i).val();
                            if(id != ""){
                                ids[c++] = id;
                            }
                        }
                    }
                }
                if(ids.length > 0){
                    admin_tool.confirm('请确认是否删除', function(){
                        $.ajax({
                            type: "GET",
                            url: "<?=Url::toRoute('admin-user/delete')?>",
                            data: {"ids":ids},
                            cache: false,
                            dataType:"json",
                            error: function (xmlHttpRequest, textStatus, errorThrown) {
                                admin_tool.alert('msg_info', '====出错了，' + textStatus, 'warning');
                            },
                            success: function(data){
                                admin_tool.alert('msg_info', '删除成功', 'success');
                                // window.location.reload();
                                app.refreshPage();
                                // this.$forceUpdate();
                                $('#data_table :checked').prop('checked',false);
                            }
                        });
                    });
                }
                else{
                    admin_tool.alert('msg_info', '请先选择要删除的数据', 'warning');
                }
            },
            editSaveClick:function(){
                $('#edit-form').submit();
            },
            loginStatus:function(s){
                var status = '';
                switch(s){
                    case '10':
                        status = '通过';
                        break;
                    case '-10':
                        status = '禁用';
                        break;
                    default:
                        status = '未审核';
                }
                return status;
            }
        },
        computed:{
            orderClass(){
                return function(field){
                    var url = window.location.search;
                    var order = "sorting";
                    url = decodeURI(url);
                    if(url.indexOf(field + " desc") > 0){
                        order = "sorting_desc";
                    }
                    else if(url.indexOf(field + " asc") > 0){
                        order = "sorting_asc";
                    }
                    return order;
                }
            }

        },
        mounted:function(){
            $('#edit-form').bind('submit', function(e) {
                e.preventDefault();
                var id = $("#id").val();
                var action = id == "" ? "<?=Url::toRoute('admin-user/create')?>" : "<?=Url::toRoute('admin-user/update')?>";
                $(this).ajaxSubmit({
                    type: "post",
                    dataType:"json",
                    url: action,
                    success: function(value)
                    {
                        if(value.errno == 0){
                            $('#edit_dialog').modal('hide');
                            admin_tool.alert('msg_info', '添加成功', 'success');
                            app.refreshPage();
                        }
                        else{
                            var json = value.data;
                            for(var key in json){
                                $('#' + key).attr({'data-placement':'bottom', 'data-content':json[key], 'data-toggle':'popover'}).addClass('popover-show').popover('show');
                            }
                        }
                    }
                });
            });
        }
    });
</script>
<?php $this->endBlock(); ?>