
<?php
use yii\bootstrap5\ActiveForm;
use yii\helpers\Url;

?>

<?php $this->beginBlock('header');  ?>
<!-- <head></head>中代码块 -->
<?php $this->endBlock(); ?>

<!-- Main content -->
<section v-cloak class="content">
    <div id="msg_info"></div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">数据列表</h3>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <button id="create_btn" @click="createClick()" type="button" class="btn btn-xs btn-primary">添&nbsp;&emsp;加</button>
                            |
                            <button id="delete_btn" @click="deleteClick()" type="button" class="btn btn-xs btn-danger">批量删除</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div id="data_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <!-- row start search-->
                        <div class="row">
                            <div class="col-sm-12">
                                <?php ActiveForm::begin(['id' => 'search-form', 'method'=>'get', 'options' => ['class' => 'form-inline'], 'action'=>Url::toRoute('admin-right/index')]); ?>                                
                    <div class="form-group" style="margin: 5px;">
                        <label>{{modelLabel.id}}:</label>
                        <input type="text" class="form-control" id="query[id]" name="query[id]"  :value="query['id']">
                    </div>
                                <div class="form-group">
                                    <a @click="searchClick" class="btn btn-primary btn-sm" href="#"> <i class="glyphicon glyphicon-zoom-in icon-white"></i>搜索</a>
                                </div>
                                <?php ActiveForm::end(); ?>                            </div>
                        </div>
                        <!-- row end search -->

                        <!-- row start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="data_table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="data_table_info">
                                    <thead class="text-nowrap">
                                    <tr role="row">
                                        <th><input id="data_table_check" type="checkbox"></th>
                                        <th @click="orderClick('id')" :class="orderClass('id')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.id}}</th>
                                        <th @click="orderClick('name')" :class="orderClass('name')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.name}}</th>
                                        <th @click="orderClick('ico')" :class="orderClass('ico')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.ico}}</th>
                                        <th @click="orderClick('weight')" :class="orderClass('weight')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.weight}}</th>
                                        <th @click="orderClick('update_user')" :class="orderClass('update_user')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.update_user}}</th>
                                        <th @click="orderClick('update_date')" :class="orderClass('update_date')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.update_date}}</th>
                                        <th tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <template v-for="m in models">
                                        <tr :id="'rowid_' + m.id">
                                            <td><label><input type="checkbox" :value="m.id"></label></td>
                                            <td>{{m.id}}</td>
                                            <td>{{m.name}}</td>
                                            <td>{{m.ico}}</td>
                                            <td>{{m.weight}}</td>
                                            <td>{{m.update_user}}</td>
                                            <td>{{m.update_date}}</td>
                                            <td class="center">
                                                <a id="edit_btn" @click="editClick(m.id)" class="btn btn-primary btn-sm" href="#"> <i class="glyphicon glyphicon-edit icon-white"></i></a>
                                                <a id="delete_btn" @click="deleteClick(m.id)" class="btn btn-danger btn-sm" href="#"> <i class="glyphicon glyphicon-trash icon-white"></i></a>
                                            </td>
                                        </tr>
                                    </template>
                                    </tbody>
                                    <!-- <tfoot></tfoot> -->
                                </table>
                            </div>
                        </div>
                        <!-- row end -->

                        <!-- row start -->
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="data_table_info" role="status" aria-live="polite">
                                    <div class="infos">
                                        从 {{startNum}}            		到  {{endNum}}            		 共 {{totalNum}} 条记录</div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="data_table_paginate">
                                    <span v-if="pages != ''" v-html="pages"></span>
                                </div>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<div class="modal fade" id="edit_dialog" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>编辑</h3>
            </div>
            <div class="modal-body">
            <?php $form = ActiveForm::begin(["id" => "edit-form", "class"=>"form-horizontal", "action"=>Url::toRoute("admin-right/save")]); ?>                
            <input type="hidden" class="form-control" id="id" name="id" v-model="model.id" />

            <div id="ico_div" class="form-group">
                <label for="ico" class="col-sm-2 control-label">{{modelLabel.ico}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="ico" name="SystemMenu[ico]" placeholder="" v-model="model.ico" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="name_div" class="form-group">
                <label for="name" class="col-sm-2 control-label">{{modelLabel.name}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="SystemMenu[name]" placeholder="" v-model="model.name" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="weight_div" class="form-group">
                <label for="weight" class="col-sm-2 control-label">{{modelLabel.weight}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="weight" name="SystemMenu[weight]" placeholder="" v-model="model.weight" />
                </div>
                <div class="clearfix"></div>
            </div>
                <div id="controller_div" class="form-group">
                    <label for="controller" class="col-sm-2 control-label">控制器ID</label>
                    <div class="col-sm-10">
                        <select @change="controllerChange" class="form-control" name="SystemFunction[controller]" id="controller" :value="model.controller">
                            <option value="">请选择</option>
                            <template v-for="(data,key) in controllerData">
                                <option :value="key" :data-key="model.controller == key ? 'true' : 'false'">{{key}}</option>
                            </template>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="actions_div" class="form-group">
                    <label for="actions" class="col-sm-2 control-label">路由地址</label>
                    <div class="col-sm-10">
                        <div id="treeview"  >
                        </div>
                    </div>
                    <div class="clearfix"><br/></div>
                </div>
                <div id="create_user_div" class="form-group">
                <label for="create_user" class="col-sm-2 control-label">{{modelLabel.create_user}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="create_user" name="SystemMenu[create_user]" placeholder="" v-model="model.create_user" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="create_date_div" class="form-group">
                <label for="create_date" class="col-sm-2 control-label">{{modelLabel.create_date}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="create_date" name="SystemMenu[create_date]" placeholder="" v-model="model.create_date" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="update_user_div" class="form-group">
                <label for="update_user" class="col-sm-2 control-label">{{modelLabel.update_user}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="update_user" name="SystemMenu[update_user]" placeholder="" v-model="model.update_user" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="update_date_div" class="form-group">
                <label for="update_date" class="col-sm-2 control-label">{{modelLabel.update_date}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="update_date" name="SystemMenu[update_date]" placeholder="" v-model="model.update_date" />
                </div>
                <div class="clearfix"></div>
            </div>
            <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">关闭</a>
                <a @click="editSaveClick" id="edit_dialog_ok" href="#" class="btn btn-primary">确定</a>
            </div>
        </div>
    </div>
</div>
<?php $this->beginBlock('footer');  ?>
<!-- <body></body>后代码块 -->
<script>
    var app = new Vue({
        el: '.content-wrapper',  //  .content
        data: {
            controllerData:<?=$controllerData?>,
            modelLabel:<?=$modelLabel?>,
            model: {controller:""},
            models:<?=$models?>,
            pages:'<?=$pages?>',
            startNum:<?=$startNum?>,
            endNum:<?=$endNum?>,
            totalNum:<?=$totalNum?>,
            query:!!<?=$query?> ? <?=$query?> : {},
        },
        methods:{
            refreshPage:function(){
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.models = [];
                        app.models = data.data.models;
                        app.pages = data.data.pages;
                        app.startNum = data.data.startNum;
                        app.endNum = data.data.endNum;
                        app.totalNum = data.data.totalNum;
                    }
                });
            },
            createClick:function(){
                app.model = {};
                $('#edit_dialog').modal('show');
            },
            searchClick:function(){
                $('#search-form').submit();
            },
            orderClick:function(field){
                var url = window.location.search;
                var optemp = field + " desc";
                if(url.indexOf('orderby') != -1){
                    url = url.replace(/orderby=([^&?]*)/ig,  function($0, $1){
                        var optemp = field + " desc";
                        optemp = decodeURI($1) != optemp ? optemp : field + " asc";
                        return "orderby=" + optemp;
                    });
                }
                else{
                    if(url.indexOf('?') != -1){
                        url = url + "&orderby=" + encodeURI(optemp);
                    }
                    else{
                        url = url + "?orderby=" + encodeURI(optemp);
                    }
                }
                window.location.href=url;
            },
            editClick:function(id){
                $.ajax({
                    type: "GET",
                    url: "<?=Url::toRoute('admin-right/view')?>",
                    data: {"id":id},
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.model = data.model;
                        $('#treeview').treeview({
                            data:data.actions,
                            showIcon: false,
                            showCheckbox: true,
                            levels: 2,
                            onNodeChecked: function(event, node) {
                                changeCheckState(node, true);
                            },
                            onNodeUnchecked: function (event, node) {
                                changeCheckState(node, false);
                            }
                        });
                        $("#create_user").attr({readonly:true,disabled:true});
                        $("#create_date").attr({readonly:true,disabled:true});
                        $("#update_user").attr({readonly:true,disabled:true});
                        $("#update_date").attr({readonly:true,disabled:true});
                        $('#edit_dialog').modal('show');
                    }
                });
            },
            deleteClick:function(id){
                var ids = [];
                if(!!id == true){
                    ids[0] = id;
                }
                else{
                    var checkboxs = $('#data_table :checked');
                    if(checkboxs.size() > 0){
                        var c = 0;
                        for(i = 0; i < checkboxs.size(); i++){
                            var id = checkboxs.eq(i).val();
                            if(id != ""){
                                ids[c++] = id;
                            }
                        }
                    }
                }
                if(ids.length > 0){
                    admin_tool.confirm('请确认是否删除', function(){
                        $.ajax({
                            type: "GET",
                            url: "<?=Url::toRoute('admin-right/delete')?>",
                            data: {"ids":ids},
                            cache: false,
                            dataType:"json",
                            error: function (xmlHttpRequest, textStatus, errorThrown) {
                                admin_tool.alert('msg_info', '====出错了，' + textStatus, 'warning');
                            },
                            success: function(data){
                                admin_tool.alert('msg_info', '删除成功', 'success');
                                // window.location.reload();
                                app.refreshPage();
                                // this.$forceUpdate();
                                $('#data_table :checked').prop('checked',false);
                            }
                        });
                    });
                }
                else{
                    admin_tool.alert('msg_info', '请先选择要删除的数据', 'warning');
                }
            },
            editSaveClick:function(){
                $('#edit-form').submit();
            },
            controllerChange:function(event){
                var controller = event.target.value;
                console.log("controller====", app.model.controller);
                console.log("controllerData====", app.controllerData);

                var actions =  app.controllerData[controller];
                var nodes = actions.nodes;
                var rightTree = {'text':controller, 'selectable':false, 'state':{'checked':false}, 'type':'r'};
                rightTree['nodes'] = nodes;

                $('#treeview').treeview({
                    data: [rightTree],
                    showIcon: false,
                    showCheckbox: true,
                    levels: 2,
                    onNodeChecked: function(event, node) {
                        //changeCheckState(node, true);
                    },
                    onNodeUnchecked: function (event, node) {
                        //changeCheckState(node, false);
                    }
                });


            }
        },
        computed:{
            orderClass(){
                return function(field){
                    var url = window.location.search;
                    var order = "sorting";
                    url = decodeURI(url);
                    if(url.indexOf(field + " desc") > 0){
                        order = "sorting_desc";
                    }
                    else if(url.indexOf(field + " asc") > 0){
                        order = "sorting_asc";
                    }
                    return order;
                }
            }
        },
        mounted:function(){
            $('#edit-form').bind('submit', function(e) {
                e.preventDefault();

                var checkNodes = $('#treeview').treeview('getChecked');
                var rightUrls = [];
                if(checkNodes.length > 0){
                    for(i = 0; i < checkNodes.length; i++){
                        var node = checkNodes[i];
                        if(node.type == 'a'){
                            var url = {'c':node.c, 'a':node.a};
                            rightUrls.push(url);
                        }
                    }
                }

                var id = $("#id").val();
                var action = id == "" ? "<?=Url::toRoute('admin-right/create')?>" : "<?=Url::toRoute('admin-right/update')?>";
                $(this).ajaxSubmit({
                    type: "post",
                    dataType:"json",
                    url: action,
                    data: {"rightUrls":rightUrls},
                    success: function(value)
                    {
                        if(value.errno == 0){
                            $('#edit_dialog').modal('hide');
                            admin_tool.alert('msg_info', '添加成功', 'success');
                            app.refreshPage();
                        }
                        else{
                            var json = value.data;
                            for(var key in json){
                                $('#' + key).attr({'data-placement':'bottom', 'data-content':json[key], 'data-toggle':'popover'}).addClass('popover-show').popover('show');
                            }
                        }
                    }
                });
            });
        }
    });

    //配置功能url
    function changeCheckState(node, checked){
        if(!!node.nodes == true){
            var nodes = node.nodes;
            for(var i = 0; i < nodes.length; i++){
                var node1 = nodes[i];
                if(checked == true){
                    $('#treeview').treeview('checkNode', [ node1.nodeId, { silent: true } ]);
                }
                else{
                    $('#treeview').treeview('uncheckNode', [ node1.nodeId, { silent: true } ]);
                }
                changeCheckState(node1, checked);
            }
        }
    }
</script>
<?php $this->endBlock(); ?>