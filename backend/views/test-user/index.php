<?php
use yii\bootstrap5\ActiveForm;
use yii\helpers\Url;
?>

<?php $this->beginBlock('header');  ?>
<!-- <head></head>中代码块 -->
<?php $this->endBlock(); ?>

<!-- Main content -->
<section v-cloak class="content">
  <div id="msg_info"></div>
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
      
        <div class="box-header">
          <h3 class="box-title">数据列表</h3>
          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
                <button id="create_btn" @click="createClick()" type="button" class="btn btn-xs btn-primary">添&nbsp;&emsp;加</button>
        			|
        		<button id="delete_btn" @click="deleteClick()" type="button" class="btn btn-xs btn-danger">批量删除</button>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
          <div id="data_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
            <!-- row start search-->
          	<div class="row">
          	<div class="col-sm-12">
                <?php ActiveForm::begin(['id' => 'search-form', 'method'=>'get', 'options' => ['class' => 'form-inline'], 'action'=>Url::toRoute('test-user/index')]); ?>

                    <div class="form-group" style="margin: 5px;">
                      <label>{{modelLabel.id}}:</label>
                      <input type="text" class="form-control" id="query[id]" name="query[id]"  :value="query['id']">
                  </div>
                    <div class="form-group" style="margin: 5px;">
                        <label>{{modelLabel.status}}:</label>
                        <input type="text" class="form-control" id="query[status]" name="query[status]"  :value="query['status']">
                    </div>
                  <div class="form-group">
              	      <a @click="searchClick" class="btn btn-primary btn-sm" href="#"> <i class="glyphicon glyphicon-zoom-in icon-white"></i>搜索</a>
           	      </div>
                <?php ActiveForm::end(); ?>
            </div>
          	</div>
          	<!-- row end search -->
          	
          	<!-- row start -->
          	<div class="row">
          	<div class="col-sm-12">
          	<table id="data_table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="data_table_info">
            <thead class="text-nowrap">
            <tr role="row">
		      <th><input id="data_table_check" type="checkbox"></th>
              <th @click="orderClick('id')" :class="orderClass('id')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.id}}</th>
              <th @click="orderClick('uname')" :class="orderClass('uname')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.uname}}</th>
              <th @click="orderClick('last_ip')" :class="orderClass('last_ip')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.last_ip}}</th>
              <th @click="orderClick('is_online')" :class="orderClass('is_online')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.is_online}}</th>
              <th @click="orderClick('status')" :class="orderClass('status')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.status}}</th>
              <th @click="orderClick('create_user')" :class="orderClass('create_user')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.create_user}}</th>
              <th @click="orderClick('create_date')" :class="orderClass('create_date')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.create_date}}</th>
              <th tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >操作</th>
            </tr>
            </thead>
            <tbody>


            <template v-for="model in models">
                <tr :id="'rowid_' + model.id">
                    <td><label><input type="checkbox" :value="model.id"></label></td>
                    <td>{{model.id}}</td>
                    <td>{{model.uname}}</td>
                    <td>{{model.last_ip}}</td>
                    <td>{{model.is_online}}</td>
                    <td>{{model.status}}</td>
                    <td>{{model.create_user}}</td>
                    <td>{{model.create_date}}</td>
                    <td class="center">
                    <a id="edit_btn" @click="editClick(model.id)" class="btn btn-primary btn-sm" > <i class="glyphicon glyphicon-edit icon-white"></i></a>
                    <a id="delete_btn" @click="deleteClick(model.id)" class="btn btn-danger btn-sm" > <i class="glyphicon glyphicon-trash icon-white"></i></a>
                    </td>
                </tr>
            </template>
           
            </tbody>
            <!-- <tfoot></tfoot> -->
          </table>
          </div>
          </div>
          <!-- row end -->
          
          <!-- row start -->
          <div class="row">
          	<div class="col-sm-5">
            	<div class="dataTables_info" id="data_table_info" role="status" aria-live="polite">
            		<div class="infos">
            		从 {{startNum}}            		到  {{endNum}}            		 共 {{totalNum}} 条记录</div>
            	</div>
            </div>
          	<div class="col-sm-7">
              	<div class="dataTables_paginate paging_simple_numbers" id="data_table_paginate">
                    <template v-if="pages != ''">
                        <span v-html="pages"></span>
                    </template>
              	</div>
          	</div>
		  </div>
		  <!-- row end -->
        </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<div class="modal fade" id="edit_dialog" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
        <?php $form = ActiveForm::begin(["id" => "edit-form", "class"=>"form-horizontal", "action"=>Url::toRoute("test-user/save")]); ?>
          <input type="hidden" class="form-control" id="id" name="id" :value="model.id"/>

          <div id="uname_div" class="form-group">
              <label for="uname" class="col-sm-2 control-label">{{modelLabel.uname}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="uname" name="TestUser[uname]" placeholder="必填" :value="model.uname"/>
              </div>
              <div class="clearfix"></div>
          </div>

          <div id="password_div" class="form-group">
              <label for="password" class="col-sm-2 control-label">{{modelLabel.password}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="password" name="TestUser[password]" placeholder="必填" :value="model.password"/>
              </div>
              <div class="clearfix"></div>
          </div>

          <div id="auth_key_div" class="form-group">
              <label for="auth_key" class="col-sm-2 control-label">{{modelLabel.auth_key}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="auth_key" name="TestUser[auth_key]" placeholder="" :value="model.auth_key"/>
              </div>
              <div class="clearfix"></div>
          </div>

          <div id="last_ip_div" class="form-group">
              <label for="last_ip" class="col-sm-2 control-label">{{modelLabel.last_ip}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="last_ip" name="TestUser[last_ip]" placeholder="" :value="model.last_ip"/>
              </div>
              <div class="clearfix"></div>
          </div>

          <div id="is_online_div" class="form-group">
              <label for="is_online" class="col-sm-2 control-label">{{modelLabel.is_online}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="is_online" name="TestUser[is_online]" placeholder="" :value="model.is_online"/>
              </div>
              <div class="clearfix"></div>
          </div>

          <div id="domain_account_div" class="form-group">
              <label for="domain_account" class="col-sm-2 control-label">{{modelLabel.domain_account}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="domain_account" name="TestUser[domain_account]" placeholder="" :value="model.domain_account"/>
              </div>
              <div class="clearfix"></div>
          </div>

          <div id="status_div" class="form-group">
              <label for="status" class="col-sm-2 control-label">{{modelLabel.status}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="status" name="TestUser[status]" placeholder="必填" :value="model.status"/>
              </div>
              <div class="clearfix"></div>
          </div>

          <div id="create_user_div" class="form-group">
              <label for="create_user" class="col-sm-2 control-label">{{modelLabel.create_user}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="create_user" name="TestUser[create_user]" placeholder="必填" :value="model.create_user"/>
              </div>
              <div class="clearfix"></div>
          </div>

          <div id="create_date_div" class="form-group">
              <label for="create_date" class="col-sm-2 control-label">{{modelLabel.create_date}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="create_date" name="TestUser[create_date]" placeholder="必填" :value="model.create_date"/>
              </div>
              <div class="clearfix"></div>
          </div>

          <div id="update_user_div" class="form-group">
              <label for="update_user" class="col-sm-2 control-label">{{modelLabel.update_user}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="update_user" name="TestUser[update_user]" placeholder="必填" :value="model.update_user"/>
              </div>
              <div class="clearfix"></div>
          </div>

          <div id="update_date_div" class="form-group">
              <label for="update_date" class="col-sm-2 control-label">{{modelLabel.update_date}}</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" id="update_date" name="TestUser[update_date]" placeholder="必填" :value="model.update_date"/>
              </div>
              <div class="clearfix"></div>
          </div>
		  <?php ActiveForm::end(); ?>
          </div>
        <div class="modal-footer">
            <a class="btn btn-default" data-dismiss="modal">关闭</a>
            <a @click="editSaveClick" id="edit_dialog_ok" href="#" class="btn btn-primary">确定</a>
        </div>
		</div>
	</div>
</div>
<?php $this->beginBlock('footer');  ?>
<!-- <body></body>后代码块 -->
<script>
    var app = new Vue({
        el: '.content-wrapper',  //  .content
        data: {
            modelLabel:<?=$modelLabel?>,
            model: {},
            models:<?=$models?>,
            pages:'<?=$pages?>',
            startNum:<?=$startNum?>,
            endNum:<?=$endNum?>,
            totalNum:<?=$totalNum?>,
            query:!!<?=$query?> ? <?=$query?> : {},
        },
        methods:{
            refreshPage:function(){
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.models = [];
                        app.models = data.data.models;
                        app.pages = data.data.pages;
                        app.startNum = data.data.startNum;
                        app.endNum = data.data.endNum;
                        app.totalNum = data.data.totalNum;
                    }
                });
            },
            createClick:function(){
                app.model = {};
                $('#edit_dialog').modal('show');
            },
            searchClick:function(){
                $('#search-form').submit();
            },
            orderClick:function(field){
                var url = window.location.search;
                var optemp = field + " desc";
                if(url.indexOf('orderby') != -1){
                    url = url.replace(/orderby=([^&?]*)/ig,  function($0, $1){
                        var optemp = field + " desc";
                        optemp = decodeURI($1) != optemp ? optemp : field + " asc";
                        return "orderby=" + optemp;
                    });
                }
                else{
                    if(url.indexOf('?') != -1){
                        url = url + "&orderby=" + encodeURI(optemp);
                    }
                    else{
                        url = url + "?orderby=" + encodeURI(optemp);
                    }
                }
                window.location.href=url;
            },
            editClick:function(id){
                $.ajax({
                    type: "GET",
                    url: "<?=Url::toRoute('test-user/view')?>",
                    data: {"id":id},
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.model = data;
                        $("#update_date").attr({readonly:false,disabled:false});
                        $('#edit_dialog').modal('show');
                    }
                });
            },
            deleteClick:function(id){
                var ids = [];
                if(!!id == true){
                    ids[0] = id;
                }
                else{
                    var checkboxs = $('#data_table :checked');
                    if(checkboxs.size() > 0){
                        var c = 0;
                        for(i = 0; i < checkboxs.size(); i++){
                            var id = checkboxs.eq(i).val();
                            if(id != ""){
                                ids[c++] = id;
                            }
                        }
                    }
                }
                if(ids.length > 0){
                    admin_tool.confirm('请确认是否删除', function(){
                        $.ajax({
                            type: "GET",
                            url: "<?=Url::toRoute('test-user/delete')?>",
                            data: {"ids":ids},
                            cache: false,
                            dataType:"json",
                            error: function (xmlHttpRequest, textStatus, errorThrown) {
                                admin_tool.alert('msg_info', '====出错了，' + textStatus, 'warning');
                            },
                            success: function(data){
                                admin_tool.alert('msg_info', '删除成功', 'success');
                                // window.location.reload();
                                app.refreshPage();
                                // this.$forceUpdate();
                                $('#data_table :checked').prop('checked',false);
                            }
                        });
                    });
                }
                else{
                    admin_tool.alert('msg_info', '请先选择要删除的数据', 'warning');
                }
            },
            editSaveClick:function(){
                $('#edit-form').submit();
            }
        },
        computed:{
            orderClass(){
                return function(field){
                    var url = window.location.search;
                    var order = "sorting";
                    url = decodeURI(url);
                    if(url.indexOf(field + " desc") > 0){
                        order = "sorting_desc";
                    }
                    else if(url.indexOf(field + " asc") > 0){
                        order = "sorting_asc";
                    }
                    return order;
                }
            }
        },
        mounted:function(){
            $('#edit-form').bind('submit', function(e) {
                e.preventDefault();
                var id = $("#id").val();
                var action = id == "" ? "<?=Url::toRoute('test-user/create')?>" : "<?=Url::toRoute('test-user/update')?>";
                $(this).ajaxSubmit({
                    type: "post",
                    dataType:"json",
                    url: action,
                    success: function(value)
                    {
                        if(value.errno == 0){
                            $('#edit_dialog').modal('hide');
                            admin_tool.alert('msg_info', '添加成功', 'success');
                            app.refreshPage();
                        }
                        else{
                            var json = value.data;
                            for(var key in json){
                                $('#' + key).attr({'data-placement':'bottom', 'data-content':json[key], 'data-toggle':'popover'}).addClass('popover-show').popover('show');
                            }
                        }
                    }
                });
            });
        }
    });
</script>
<?php $this->endBlock(); ?>