
<?php
use yii\bootstrap5\ActiveForm;
use yii\helpers\Url;

?>

<?php $this->beginBlock('header');  ?>
<!-- <head></head>中代码块 -->
<?php $this->endBlock(); ?>

<!-- Main content -->
<section v-cloak class="content">
    <div id="msg_info"></div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">菜单管理</h3>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <button id="create_btn" @click="createClick()" type="button" class="btn btn-xs btn-primary">添&nbsp;&emsp;加</button>
                            |
                            <button id="delete_btn" @click="deleteClick()" type="button" class="btn btn-xs btn-danger">批量删除</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div id="data_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <!-- row start search-->
                        <div class="row">
                            <div class="col-sm-12">
                                <?php ActiveForm::begin(['id' => 'search-form', 'method'=>'get', 'options' => ['class' => 'form-inline'], 'action'=>Url::toRoute('admin-menu/index')]); ?>                                
                    <div class="form-group" style="margin: 5px;">
                        <label>{{modelLabel.id}}:</label>
                        <input type="text" class="form-control" id="query[id]" name="query[id]"  :value="query['id']">
                    </div>

                    <div class="form-group" style="margin: 5px;">
                        <label>{{modelLabel.name}}:</label>
                        <input type="text" class="form-control" id="query[name]" name="query[name]"  :value="query['name']">
                    </div>
                                <div class="form-group">
                                    <a @click="searchClick" class="btn btn-primary btn-sm" href="#"> <i class="glyphicon glyphicon-zoom-in icon-white"></i>搜索</a>
                                </div>
                                <?php ActiveForm::end(); ?>                            </div>
                        </div>
                        <!-- row end search -->

                        <!-- row start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="data_table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="data_table_info">
                                    <thead class="text-nowrap">
                                    <tr role="row">
                                        <th><input id="data_table_check" type="checkbox"></th>
                                        <th @click="orderClick('name')" :class="orderClass('name')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.name}}</th>
                                        <th @click="orderClick('ico')" :class="orderClass('ico')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.ico}}</th>
                                        <th @click="orderClick('weight')" :class="orderClass('weight')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.weight}}</th>
                                        <th @click="orderClick('update_user')" :class="orderClass('update_user')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.update_user}}</th>
                                        <th @click="orderClick('update_date')" :class="orderClass('update_date')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.update_date}}</th>
                                        <th tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <template v-for="m in models">
                                        <tr :id="'rowid_' + m.id">
                                            <td><label><input type="checkbox" :value="m.id"></label></td>
                                            <td>{{m.name}}</td>
                                            <td>{{m.ico}}</td>
                                            <td>{{m.weight}}</td>
                                            <td>{{m.update_user}}</td>
                                            <td>{{m.update_date}}</td>
                                            <td class="center">
                                                <a class="btn btn-primary btn-sm" :href="'<?=Url::toRoute(['admin-right/index'])?>&query[pid]=' + m.id" title="权限管理"> <i class="glyphicon glyphicon-th-list"></i></a>
                                                <a id="edit_btn" @click="editClick(m.id)" class="btn btn-primary btn-sm" href="#" title="修改"> <i class="glyphicon glyphicon-edit icon-white"></i></a>
                                                <a id="delete_btn" @click="deleteClick(m.id)" class="btn btn-danger btn-sm" href="#" title="删除"> <i class="glyphicon glyphicon-trash icon-white"></i></a>
                                            </td>
                                        </tr>
                                    </template>
                                    </tbody>
                                    <!-- <tfoot></tfoot> -->
                                </table>
                            </div>
                        </div>
                        <!-- row end -->

                        <!-- row start -->
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="data_table_info" role="status" aria-live="polite">
                                    <div class="infos">
                                        从 {{startNum}}            		到  {{endNum}}            		 共 {{totalNum}} 条记录</div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="data_table_paginate">
                                    <span v-if="pages != ''" v-html="pages"></span>
                                </div>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<div class="modal fade" id="edit_dialog" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>编辑</h3>
            </div>
            <div class="modal-body">
            <?php $form = ActiveForm::begin(["id" => "edit-form", "class"=>"form-horizontal", "action"=>Url::toRoute("admin-menu/save")]); ?>                
            <input type="hidden" class="form-control" id="id" name="id" v-model="model.id" />

            <div id="ico_div" class="form-group">
                <label for="ico" class="col-sm-2 control-label">{{modelLabel.ico}}</label>
                <div class="col-sm-10">
                    <div class="input-group input-groupp-md">
                        <input type="text" class="form-control" id="icon" name="SystemMenu[ico]" placeholder="" :value="!!model.ico ? model.ico : 'glyphicon glyphicon-unchecked'" />
                        <a @click="searchIcoClick" href="javascript:;" class="btn-search-icon input-group-addon"><span id="ico_preview" :class="!!model.ico ? model.ico : 'glyphicon glyphicon-unchecked'"></span>&emsp;选择</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <input type="hidden" class="form-control" id="name" name="name" v-model="model.name" />

            <div id="entry_url_div" class="form-group">
                <label for="entry_url" class="col-sm-2 control-label">{{modelLabel.entry_url}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="entry_url" name="SystemMenu[entry_url]" placeholder="" v-model="model.entry_url" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="weight_div" class="form-group">
                <label for="weight" class="col-sm-2 control-label">{{modelLabel.weight}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="weight" name="SystemMenu[weight]" placeholder="" :value="!!model.weight ? model.weight : 0" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="create_user_div" class="form-group">
                <label for="create_user" class="col-sm-2 control-label">{{modelLabel.create_user}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="create_user" name="SystemMenu[create_user]" placeholder="" v-model="model.create_user" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="create_date_div" class="form-group">
                <label for="create_date" class="col-sm-2 control-label">{{modelLabel.create_date}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="create_date" name="SystemMenu[create_date]" placeholder="" v-model="model.create_date" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="update_user_div" class="form-group">
                <label for="update_user" class="col-sm-2 control-label">{{modelLabel.update_user}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="update_user" name="SystemMenu[update_user]" placeholder="" v-model="model.update_user" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="update_date_div" class="form-group">
                <label for="update_date" class="col-sm-2 control-label">{{modelLabel.update_date}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="update_date" name="SystemMenu[update_date]" placeholder="" v-model="model.update_date" />
                </div>
                <div class="clearfix"></div>
            </div>
            <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">关闭</a>
                <a @click="editSaveClick" id="edit_dialog_ok" href="#" class="btn btn-primary">确定</a>
            </div>
        </div>
    </div>
</div>
<?=Yii::$app->view->renderFile('@app/views/admin-module/ico.php'); ?>

<?php $this->beginBlock('footer');  ?>
<!-- <body></body>后代码块 -->
<script>
    var app = new Vue({
        el: '.content-wrapper',  //  .content
        data: {
            modelLabel:<?=$modelLabel?>,
            model: {},
            models:<?=$models?>,
            pages:'<?=$pages?>',
            startNum:<?=$startNum?>,
            endNum:<?=$endNum?>,
            totalNum:<?=$totalNum?>,
            query:!!<?=$query?> ? <?=$query?> : {},
        },
        methods:{
            refreshPage:function(){
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.models = [];
                        app.models = data.data.models;
                        app.pages = data.data.pages;
                        app.startNum = data.data.startNum;
                        app.endNum = data.data.endNum;
                        app.totalNum = data.data.totalNum;
                    }
                });
            },
            createClick:function(){
                app.model = {};
                $("#create_user").parent().parent().hide();
                $("#create_date").parent().parent().hide();
                $("#update_user").parent().parent().hide();
                $("#update_date").parent().parent().hide();
                $('#edit_dialog').modal('show');
            },
            searchClick:function(){
                $('#search-form').submit();
            },
            orderClick:function(field){
                var url = window.location.search;
                var optemp = field + " desc";
                if(url.indexOf('orderby') != -1){
                    url = url.replace(/orderby=([^&?]*)/ig,  function($0, $1){
                        var optemp = field + " desc";
                        optemp = decodeURI($1) != optemp ? optemp : field + " asc";
                        return "orderby=" + optemp;
                    });
                }
                else{
                    if(url.indexOf('?') != -1){
                        url = url + "&orderby=" + encodeURI(optemp);
                    }
                    else{
                        url = url + "?orderby=" + encodeURI(optemp);
                    }
                }
                window.location.href=url;
            },
            editClick:function(id){
                $.ajax({
                    type: "GET",
                    url: "<?=Url::toRoute('admin-menu/view')?>",
                    data: {"id":id},
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.model = data;
                        $("#create_user").attr({readonly:true,disabled:true}).parent().parent().show();
                        $("#create_date").attr({readonly:true,disabled:true}).parent().parent().show();
                        $("#update_user").attr({readonly:true,disabled:true}).parent().parent().show();
                        $("#update_date").attr({readonly:true,disabled:true}).parent().parent().show();
                        $('#edit_dialog').modal('show');
                    }
                });
            },
            deleteClick:function(id){
                var ids = [];
                if(!!id == true){
                    ids[0] = id;
                }
                else{
                    var checkboxs = $('#data_table :checked');
                    if(checkboxs.size() > 0){
                        var c = 0;
                        for(i = 0; i < checkboxs.size(); i++){
                            var id = checkboxs.eq(i).val();
                            if(id != ""){
                                ids[c++] = id;
                            }
                        }
                    }
                }
                if(ids.length > 0){
                    admin_tool.confirm('请确认是否删除', function(){
                        $.ajax({
                            type: "GET",
                            url: "<?=Url::toRoute('admin-menu/delete')?>",
                            data: {"ids":ids},
                            cache: false,
                            dataType:"json",
                            error: function (xmlHttpRequest, textStatus, errorThrown) {
                                admin_tool.alert('msg_info', '====出错了，' + textStatus, 'warning');
                            },
                            success: function(data){
                                admin_tool.alert('msg_info', '删除成功', 'success');
                                // window.location.reload();
                                app.refreshPage();
                                // this.$forceUpdate();
                                $('#data_table :checked').prop('checked',false);
                            }
                        });
                    });
                }
                else{
                    admin_tool.alert('msg_info', '请先选择要删除的数据', 'warning');
                }
            },
            editSaveClick:function(){
                $('#edit-form').submit();
            },
            searchIcoClick:function(event){
                $('#ico_dialog').modal('show');
            }
        },
        computed:{
            orderClass(){
                return function(field){
                    var url = window.location.search;
                    var order = "sorting";
                    url = decodeURI(url);
                    if(url.indexOf(field + " desc") > 0){
                        order = "sorting_desc";
                    }
                    else if(url.indexOf(field + " asc") > 0){
                        order = "sorting_asc";
                    }
                    return order;
                }
            }
        },
        mounted:function(){
            $('#edit-form').bind('submit', function(e) {
                e.preventDefault();
                var id = $("#id").val();
                var action = id == "" ? "<?=Url::toRoute('admin-menu/create')?>" : "<?=Url::toRoute('admin-menu/update')?>";
                $(this).ajaxSubmit({
                    type: "post",
                    dataType:"json",
                    url: action,
                    success: function(value)
                    {
                        if(value.errno == 0){
                            $('#edit_dialog').modal('hide');
                            admin_tool.alert('msg_info', '添加成功', 'success');
                            app.refreshPage();
                        }
                        else{
                            var json = value.data;
                            for(var key in json){
                                $('#' + key).attr({'data-placement':'bottom', 'data-content':json[key], 'data-toggle':'popover'}).addClass('popover-show').popover('show');
                            }
                        }
                    }
                });
            });
            $('#ico_dialog .bs-glyphicons li').click(function(){
                var span = $(this).find('span');
                var css = span.attr('class');
                app.model.ico = css;
                $('#ico_preview').attr('class', css);
                $('#ico_dialog').modal('hide');
            });
        }
    });
</script>
<?php $this->endBlock(); ?>