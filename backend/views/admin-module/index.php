
<?php
use yii\bootstrap5\ActiveForm;
use yii\helpers\Url;

?>

<?php $this->beginBlock('header');  ?>
<!-- <head></head>中代码块 -->
<style>
    .bs-glyphicons {
        list-style: none;
        overflow: hidden;
    }
    .bs-glyphicons li:hover {
        background-color: rgba(86, 61, 124, .1);
    }
    .bs-glyphicons li {
        float: left;
        width: 40px;
        height: 41px;
        margin: 1px;
        font-size: 18px;
        line-height: 41px;
        text-align: center;
        border: 1px solid #ddd;
    }
</style>
<?php $this->endBlock(); ?>

<!-- Main content -->
<section v-cloak class="content">
    <div id="msg_info"></div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">模块管理</h3>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <button id="create_btn" @click="createClick()" type="button" class="btn btn-xs btn-primary">添&nbsp;&emsp;加</button>
                            |
                            <button id="delete_btn" @click="deleteClick()" type="button" class="btn btn-xs btn-danger">批量删除</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div id="data_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <!-- row start search-->
                        <div class="row">
                            <div class="col-sm-12">
                                <?php ActiveForm::begin(['id' => 'search-form', 'method'=>'get', 'options' => ['class' => 'form-inline'], 'action'=>Url::toRoute('admin-module/index')]); ?>                                
                    <div class="form-group" style="margin: 5px;">
                        <label>{{modelLabel.id}}:</label>
                        <input type="text" class="form-control" id="query[id]" name="query[id]"  :value="query['id']">
                    </div>

                    <div class="form-group" style="margin: 5px;">
                        <label>{{modelLabel.name}}:</label>
                        <input type="text" class="form-control" id="query[name]" name="query[name]"  :value="query['name']">
                    </div>
                                <div class="form-group">
                                    <a @click="searchClick" class="btn btn-primary btn-sm" href="#"> <i class="glyphicon glyphicon-zoom-in icon-white"></i>搜索</a>
                                </div>
                                <?php ActiveForm::end(); ?>                            </div>
                        </div>
                        <!-- row end search -->

                        <!-- row start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="data_table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="data_table_info">
                                    <thead class="text-nowrap">
                                    <tr role="row">
                                        <th><input id="data_table_check" type="checkbox"></th>
                                        <th @click="orderClick('id')" :class="orderClass('id')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.id}}</th>
                                        <th @click="orderClick('ico')" :class="orderClass('ico')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.ico}}</th>
                                        <th @click="orderClick('name')" :class="orderClass('name')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.name}}</th>
                                        <th @click="orderClick('weight')" :class="orderClass('weight')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.weight}}</th>
                                        <th @click="orderClick('update_user')" :class="orderClass('update_user')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.update_user}}</th>
                                        <th @click="orderClick('update_date')" :class="orderClass('update_date')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.update_date}}</th>
                                        <th tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <template v-for="m in models">
                                        <tr :id="'rowid_' + m.id">
                                            <td><label><input type="checkbox" :value="m.id"></label></td>
                                            <td>{{m.id}}</td>
                                            <td><span :class="m.ico"></span></td>
                                            <td>{{m.name}}</td>
                                            <td>{{m.weight}}</td>
                                            <td>{{m.update_user}}</td>
                                            <td>{{m.update_date}}</td>
                                            <td class="center">
                                                <a class="btn btn-primary btn-sm" :href="'<?=Url::toRoute(['admin-menu/index'])?>&query[pid]=' + m.id" title="菜单"> <i class="glyphicon glyphicon-menu-hamburger"></i></a>
                                                <a id="edit_btn" @click="editClick(m.id)" class="btn btn-primary btn-sm" href="#" title="修改"> <i class="glyphicon glyphicon-edit icon-white"></i></a>
                                                <a id="delete_btn" @click="deleteClick(m.id)" class="btn btn-danger btn-sm" href="#" title="删除"> <i class="glyphicon glyphicon-trash icon-white"></i></a>
                                            </td>
                                        </tr>
                                    </template>
                                    </tbody>
                                    <!-- <tfoot></tfoot> -->
                                </table>
                            </div>
                        </div>
                        <!-- row end -->

                        <!-- row start -->
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="data_table_info" role="status" aria-live="polite">
                                    <div class="infos">
                                        从 {{startNum}}            		到  {{endNum}}            		 共 {{totalNum}} 条记录</div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="data_table_paginate">
                                    <span v-if="pages != ''" v-html="pages"></span>
                                </div>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<div class="modal fade" id="edit_dialog" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">编辑</h4>
            </div>
            <div class="modal-body">
            <?php $form = ActiveForm::begin(["id" => "edit-form", "class"=>"form-horizontal", "action"=>Url::toRoute("admin-module/save")]); ?>                

            <input type="hidden" class="form-control" id="id" name="id" v-model="model.id" />

            <div id="name_div" class="form-group">
                <label for="name" class="col-sm-2 control-label">{{modelLabel.name}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="SystemMenu[name]" placeholder="" v-model="model.name" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="type_div" class="form-group">
                <label for="type" class="col-sm-2 control-label">{{modelLabel.type}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="type" name="SystemMenu[type]" placeholder="" v-model="model.type" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="ico_div" class="form-group">
                <label for="ico" class="col-sm-2 control-label">{{modelLabel.ico}}</label>
                <div class="col-sm-10">
                    <div class="input-group input-groupp-md">

                        <input type="text" class="form-control" id="icon" name="SystemMenu[ico]" placeholder="" v-model="model.ico" />
                        <a @click="searchIcoClick" href="javascript:;" class="btn-search-icon input-group-addon"><span id="ico_preview"></span>&emsp;选择</a>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>


            <div id="entry_url_div" class="form-group">
                <label for="entry_url" class="col-sm-2 control-label">{{modelLabel.entry_url}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="entry_url" name="SystemMenu[entry_url]" placeholder="" v-model="model.entry_url" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="weight_div" class="form-group">
                <label for="weight" class="col-sm-2 control-label">{{modelLabel.weight}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="weight" name="SystemMenu[weight]" placeholder="" v-model="model.weight" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="create_user_div" class="form-group">
                <label for="create_user" class="col-sm-2 control-label">{{modelLabel.create_user}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="create_user" name="SystemMenu[create_user]" placeholder="" v-model="model.create_user" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="create_date_div" class="form-group">
                <label for="create_date" class="col-sm-2 control-label">{{modelLabel.create_date}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="create_date" name="SystemMenu[create_date]" placeholder="" v-model="model.create_date" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="update_user_div" class="form-group">
                <label for="update_user" class="col-sm-2 control-label">{{modelLabel.update_user}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="update_user" name="SystemMenu[update_user]" placeholder="" v-model="model.update_user" />
                </div>
                <div class="clearfix"></div>
            </div>

            <div id="update_date_div" class="form-group">
                <label for="update_date" class="col-sm-2 control-label">{{modelLabel.update_date}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="update_date" name="SystemMenu[update_date]" placeholder="" v-model="model.update_date" />
                </div>
                <div class="clearfix"></div>
            </div>
            <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">关闭</a>
                <a @click="editSaveClick" id="edit_dialog_ok" href="#" class="btn btn-primary">确定</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ico_dialog" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">图标选择</h4>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(["id" => "edit-form", "class"=>"form-horizontal", "action"=>Url::toRoute("admin-module/save")]); ?>

                <ul class="bs-glyphicons">
                    <li>
                        <span title="glyphicon glyphicon-asterisk" class="glyphicon glyphicon-asterisk"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-plus" class="glyphicon glyphicon-plus"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-euro" class="glyphicon glyphicon-euro"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-eur" class="glyphicon glyphicon-eur"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-minus" class="glyphicon glyphicon-minus"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-cloud" class="glyphicon glyphicon-cloud"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-envelope" class="glyphicon glyphicon-envelope"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-pencil" class="glyphicon glyphicon-pencil"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-glass" class="glyphicon glyphicon-glass"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-music" class="glyphicon glyphicon-music"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-search" class="glyphicon glyphicon-search"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-heart" class="glyphicon glyphicon-heart"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-star" class="glyphicon glyphicon-star"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-star-empty" class="glyphicon glyphicon-star-empty"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-user" class="glyphicon glyphicon-user"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-film" class="glyphicon glyphicon-film"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-th-large" class="glyphicon glyphicon-th-large"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-th" class="glyphicon glyphicon-th"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-th-list" class="glyphicon glyphicon-th-list"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-ok" class="glyphicon glyphicon-ok"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-remove" class="glyphicon glyphicon-remove"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-zoom-in" class="glyphicon glyphicon-zoom-in"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-zoom-out" class="glyphicon glyphicon-zoom-out"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-off" class="glyphicon glyphicon-off"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-signal" class="glyphicon glyphicon-signal"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-cog" class="glyphicon glyphicon-cog"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-trash" class="glyphicon glyphicon-trash"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-home" class="glyphicon glyphicon-home"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-file" class="glyphicon glyphicon-file"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-time" class="glyphicon glyphicon-time"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-road" class="glyphicon glyphicon-road"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-download-alt" class="glyphicon glyphicon-download-alt"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-download" class="glyphicon glyphicon-download"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-upload" class="glyphicon glyphicon-upload"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-inbox" class="glyphicon glyphicon-inbox"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-play-circle" class="glyphicon glyphicon-play-circle"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-repeat" class="glyphicon glyphicon-repeat"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-refresh" class="glyphicon glyphicon-refresh"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-list-alt" class="glyphicon glyphicon-list-alt"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-lock" class="glyphicon glyphicon-lock"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-flag" class="glyphicon glyphicon-flag"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-headphones" class="glyphicon glyphicon-headphones"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-volume-off" class="glyphicon glyphicon-volume-off"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-volume-down" class="glyphicon glyphicon-volume-down"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-volume-up" class="glyphicon glyphicon-volume-up"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-qrcode" class="glyphicon glyphicon-qrcode"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-barcode" class="glyphicon glyphicon-barcode"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-tag" class="glyphicon glyphicon-tag"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-tags" class="glyphicon glyphicon-tags"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-book" class="glyphicon glyphicon-book"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-bookmark" class="glyphicon glyphicon-bookmark"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-print" class="glyphicon glyphicon-print"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-camera" class="glyphicon glyphicon-camera"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-font" class="glyphicon glyphicon-font"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-bold" class="glyphicon glyphicon-bold"></span>
                    </li>
                    <li>
                        <span title="glyphicon glyphicon-italic" class="glyphicon glyphicon-italic"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-text-height" title="glyphicon glyphicon-text-height"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-text-width" title="glyphicon glyphicon-text-width"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-align-left" title="glyphicon glyphicon-align-left"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-align-center" title="glyphicon glyphicon-align-center"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-align-right" title="glyphicon glyphicon-align-right"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-align-justify" title="glyphicon glyphicon-align-justify"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-list" title="glyphicon glyphicon-list"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-indent-left" title="glyphicon glyphicon-indent-left"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-indent-right" title="glyphicon glyphicon-indent-right"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-facetime-video" title="glyphicon glyphicon-facetime-video"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-picture" title="glyphicon glyphicon-picture"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-map-marker" title="glyphicon glyphicon-map-marker"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-adjust" title="glyphicon glyphicon-adjust"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-tint" title="glyphicon glyphicon-tint"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-edit" title="glyphicon glyphicon-edit"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-share" title="glyphicon glyphicon-share"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-check" title="glyphicon glyphicon-check"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-move" title="glyphicon glyphicon-move"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-step-backward" title="glyphicon glyphicon-step-backward"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-fast-backward" title="glyphicon glyphicon-fast-backward"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-backward" title="glyphicon glyphicon-backward"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-play" title="glyphicon glyphicon-play"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-pause" title="glyphicon glyphicon-pause"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-stop" title="glyphicon glyphicon-stop"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-forward" title="glyphicon glyphicon-forward"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-fast-forward" title="glyphicon glyphicon-fast-forward"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-step-forward" title="glyphicon glyphicon-step-forward"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-eject" title="glyphicon glyphicon-eject"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-chevron-left" title="glyphicon glyphicon-chevron-left"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-chevron-right" title="glyphicon glyphicon-chevron-right"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-plus-sign" title="glyphicon glyphicon-plus-sign"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-minus-sign" title="glyphicon glyphicon-minus-sign"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-remove-sign" title="glyphicon glyphicon-remove-sign"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-ok-sign" title="glyphicon glyphicon-ok-sign"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-question-sign" title="glyphicon glyphicon-question-sign"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-info-sign" title="glyphicon glyphicon-info-sign"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-screenshot" title="glyphicon glyphicon-screenshot"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-remove-circle" title="glyphicon glyphicon-remove-circle"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-ok-circle" title="glyphicon glyphicon-ok-circle"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-ban-circle" title="glyphicon glyphicon-ban-circle"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-arrow-left" title="glyphicon glyphicon-arrow-left"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-arrow-right" title="glyphicon glyphicon-arrow-right"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-arrow-up" title="glyphicon glyphicon-arrow-up"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-arrow-down" title="glyphicon glyphicon-arrow-down"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-share-alt" title="glyphicon glyphicon-share-alt"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-resize-full" title="glyphicon glyphicon-resize-full"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-resize-small" title="glyphicon glyphicon-resize-small"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-exclamation-sign" title="glyphicon glyphicon-exclamation-sign"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-gift" title="glyphicon glyphicon-gift"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-leaf" title="glyphicon glyphicon-leaf"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-fire" title="glyphicon glyphicon-fire"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-eye-open" title="glyphicon glyphicon-eye-open"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-eye-close" title="glyphicon glyphicon-eye-close"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-warning-sign" title="glyphicon glyphicon-warning-sign"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-plane" title="glyphicon glyphicon-plane"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-calendar" title="glyphicon glyphicon-calendar"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-random" title="glyphicon glyphicon-random"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-comment" title="glyphicon glyphicon-comment"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-magnet" title="glyphicon glyphicon-magnet"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-chevron-up" title="glyphicon glyphicon-chevron-up"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-chevron-down" title="glyphicon glyphicon-chevron-down"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-retweet" title="glyphicon glyphicon-retweet"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-shopping-cart" title="glyphicon glyphicon-shopping-cart"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-folder-close" title="glyphicon glyphicon-folder-close"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-folder-open" title="glyphicon glyphicon-folder-open"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-resize-vertical" title="glyphicon glyphicon-resize-vertical"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-resize-horizontal" title="glyphicon glyphicon-resize-horizontal"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-hdd" title="glyphicon glyphicon-hdd"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-bullhorn" title="glyphicon glyphicon-bullhorn"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-bell" title="glyphicon glyphicon-bell"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-certificate" title="glyphicon glyphicon-certificate"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-thumbs-up" title="glyphicon glyphicon-thumbs-up"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-thumbs-down" title="glyphicon glyphicon-thumbs-down"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-hand-right" title="glyphicon glyphicon-hand-right"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-hand-left" title="glyphicon glyphicon-hand-left"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-hand-up" title="glyphicon glyphicon-hand-up"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-hand-down" title="glyphicon glyphicon-hand-down"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-circle-arrow-right" title="glyphicon glyphicon-circle-arrow-right"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-circle-arrow-left" title="glyphicon glyphicon-circle-arrow-left"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-circle-arrow-up" title="glyphicon glyphicon-circle-arrow-up"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-circle-arrow-down" title="glyphicon glyphicon-circle-arrow-down"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-globe" title="glyphicon glyphicon-globe"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-wrench" title="glyphicon glyphicon-wrench"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-tasks" title="glyphicon glyphicon-tasks"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-filter" title="glyphicon glyphicon-filter"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-briefcase" title="glyphicon glyphicon-briefcase"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-fullscreen" title="glyphicon glyphicon-fullscreen"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-dashboard" title="glyphicon glyphicon-dashboard"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-paperclip" title="glyphicon glyphicon-paperclip"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-heart-empty" title="glyphicon glyphicon-heart-empty"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-link" title="glyphicon glyphicon-link"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-phone" title="glyphicon glyphicon-phone"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-pushpin" title="glyphicon glyphicon-pushpin"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-usd" title="glyphicon glyphicon-usd"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-gbp" title="glyphicon glyphicon-gbp"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sort" title="glyphicon glyphicon-sort"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sort-by-alphabet" title="glyphicon glyphicon-sort-by-alphabet"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sort-by-alphabet-alt" title="glyphicon glyphicon-sort-by-alphabet-alt"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sort-by-order" title="glyphicon glyphicon-sort-by-order"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sort-by-order-alt" title="glyphicon glyphicon-sort-by-order-alt"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sort-by-attributes" title="glyphicon glyphicon-sort-by-attributes"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sort-by-attributes-alt" title="glyphicon glyphicon-sort-by-attributes-alt"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-unchecked" title="glyphicon glyphicon-unchecked"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-expand" title="glyphicon glyphicon-expand"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-collapse-down" title="glyphicon glyphicon-collapse-down"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-collapse-up" title="glyphicon glyphicon-collapse-up"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-log-in" title="glyphicon glyphicon-log-in"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-flash" title="glyphicon glyphicon-flash"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-log-out" title="glyphicon glyphicon-log-out"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-new-window" title="glyphicon glyphicon-new-window"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-record" title="glyphicon glyphicon-record"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-save" title="glyphicon glyphicon-save"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-open" title="glyphicon glyphicon-open"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-saved" title="glyphicon glyphicon-saved"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-import" title="glyphicon glyphicon-import"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-export" title="glyphicon glyphicon-export"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-send" title="glyphicon glyphicon-send"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-floppy-disk" title="glyphicon glyphicon-floppy-disk"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-floppy-saved" title="glyphicon glyphicon-floppy-saved"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-floppy-remove" title="glyphicon glyphicon-floppy-remove"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-floppy-save" title="glyphicon glyphicon-floppy-save"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-floppy-open" title="glyphicon glyphicon-floppy-open"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-credit-card" title="glyphicon glyphicon-credit-card"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-transfer" title="glyphicon glyphicon-transfer"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-cutlery" title="glyphicon glyphicon-cutlery"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-header" title="glyphicon glyphicon-header"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-compressed" title="glyphicon glyphicon-compressed"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-earphone" title="glyphicon glyphicon-earphone"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-phone-alt" title="glyphicon glyphicon-phone-alt"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-tower" title="glyphicon glyphicon-tower"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-stats" title="glyphicon glyphicon-stats"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sd-video" title="glyphicon glyphicon-sd-video"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-hd-video" title="glyphicon glyphicon-hd-video"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-subtitles" title="glyphicon glyphicon-subtitles"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sound-stereo" title="glyphicon glyphicon-sound-stereo"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sound-dolby" title="glyphicon glyphicon-sound-dolby"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sound-5-1" title="glyphicon glyphicon-sound-5-1"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sound-6-1" title="glyphicon glyphicon-sound-6-1"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sound-7-1" title="glyphicon glyphicon-sound-7-1"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-copyright-mark" title="glyphicon glyphicon-copyright-mark"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-registration-mark" title="glyphicon glyphicon-registration-mark"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-cloud-download" title="glyphicon glyphicon-cloud-download"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-cloud-upload" title="glyphicon glyphicon-cloud-upload"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-tree-conifer" title="glyphicon glyphicon-tree-conifer"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-tree-deciduous" title="glyphicon glyphicon-tree-deciduous"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-cd" title="glyphicon glyphicon-cd"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-save-file" title="glyphicon glyphicon-save-file"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-open-file" title="glyphicon glyphicon-open-file"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-level-up" title="glyphicon glyphicon-level-up"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-copy" title="glyphicon glyphicon-copy"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-paste" title="glyphicon glyphicon-paste"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-alert" title="glyphicon glyphicon-alert"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-equalizer" title="glyphicon glyphicon-equalizer"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-king" title="glyphicon glyphicon-king"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-queen" title="glyphicon glyphicon-queen"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-pawn" title="glyphicon glyphicon-pawn"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-bishop" title="glyphicon glyphicon-bishop"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-knight" title="glyphicon glyphicon-knight"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-baby-formula" title="glyphicon glyphicon-baby-formula"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-tent" title="glyphicon glyphicon-tent"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-blackboard" title="glyphicon glyphicon-blackboard"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-bed" title="glyphicon glyphicon-bed"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-apple" title="glyphicon glyphicon-apple"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-erase" title="glyphicon glyphicon-erase"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-hourglass" title="glyphicon glyphicon-hourglass"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-lamp" title="glyphicon glyphicon-lamp"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-duplicate" title="glyphicon glyphicon-duplicate"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-piggy-bank" title="glyphicon glyphicon-piggy-bank"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-scissors" title="glyphicon glyphicon-scissors"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-bitcoin" title="glyphicon glyphicon-bitcoin"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-btc" title="glyphicon glyphicon-btc"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-xbt" title="glyphicon glyphicon-xbt"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-yen" title="glyphicon glyphicon-yen"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-jpy" title="glyphicon glyphicon-jpy"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-ruble" title="glyphicon glyphicon-ruble"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-rub" title="glyphicon glyphicon-rub"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-scale" title="glyphicon glyphicon-scale"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-ice-lolly" title="glyphicon glyphicon-ice-lolly"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-ice-lolly-tasted" title="glyphicon glyphicon-ice-lolly-tasted"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-education" title="glyphicon glyphicon-education"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-option-horizontal" title="glyphicon glyphicon-option-horizontal"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-option-vertical" title="glyphicon glyphicon-option-vertical"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-menu-hamburger" title="glyphicon glyphicon-menu-hamburger"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-modal-window" title="glyphicon glyphicon-modal-window"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-oil" title="glyphicon glyphicon-oil"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-grain" title="glyphicon glyphicon-grain"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-sunglasses" title="glyphicon glyphicon-sunglasses"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-text-size" title="glyphicon glyphicon-text-size"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-text-color" title="glyphicon glyphicon-text-color"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-text-background" title="glyphicon glyphicon-text-background"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-object-align-top" title="glyphicon glyphicon-object-align-top"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-object-align-bottom" title="glyphicon glyphicon-object-align-bottom"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-object-align-horizontal" title="glyphicon glyphicon-object-align-horizontal"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-object-align-left" title="glyphicon glyphicon-object-align-left"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-object-align-vertical" title="glyphicon glyphicon-object-align-vertical"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-object-align-right" title="glyphicon glyphicon-object-align-right"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-triangle-right" title="glyphicon glyphicon-triangle-right"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-triangle-left" title="glyphicon glyphicon-triangle-left"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-triangle-bottom" title="glyphicon glyphicon-triangle-bottom"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-triangle-top" title="glyphicon glyphicon-triangle-top"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-console" title="glyphicon glyphicon-console"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-superscript" title="glyphicon glyphicon-superscript"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-subscript" title="glyphicon glyphicon-subscript"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-menu-left" title="glyphicon glyphicon-menu-left"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-menu-right" title="glyphicon glyphicon-menu-right"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-menu-down" title="glyphicon glyphicon-menu-down"></span>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-menu-up" title="glyphicon glyphicon-menu-up"></span>
                    </li>
                </ul>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">关闭</a>
            </div>
        </div>
    </div>
</div>
<?php $this->beginBlock('footer');  ?>
<!-- <body></body>后代码块 -->
<script>
    var app = new Vue({
        el: '.content-wrapper',
        data: {
            modelLabel:<?=$modelLabel?>,
            model: {},
            models:<?=$models?>,
            pages:'<?=$pages?>',
            startNum:<?=$startNum?>,
            endNum:<?=$endNum?>,
            totalNum:<?=$totalNum?>,
            query:!!<?=$query?> ? <?=$query?> : {},
        },
        methods:{
            refreshPage:function(){
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.models = [];
                        app.models = data.data.models;
                        app.pages = data.data.pages;
                        app.startNum = data.data.startNum;
                        app.endNum = data.data.endNum;
                        app.totalNum = data.data.totalNum;
                    }
                });
            },
            createClick:function(){
                app.model = {};
                $('#edit_dialog').modal('show');
            },
            searchClick:function(){
                $('#search-form').submit();
            },
            orderClick:function(field){
                var url = window.location.search;
                var optemp = field + " desc";
                if(url.indexOf('orderby') != -1){
                    url = url.replace(/orderby=([^&?]*)/ig,  function($0, $1){
                        var optemp = field + " desc";
                        optemp = decodeURI($1) != optemp ? optemp : field + " asc";
                        return "orderby=" + optemp;
                    });
                }
                else{
                    if(url.indexOf('?') != -1){
                        url = url + "&orderby=" + encodeURI(optemp);
                    }
                    else{
                        url = url + "?orderby=" + encodeURI(optemp);
                    }
                }
                window.location.href=url;
            },
            editClick:function(id){
                $.ajax({
                    type: "GET",
                    url: "<?=Url::toRoute('admin-module/view')?>",
                    data: {"id":id},
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.model = data;
                        $("#create_user").attr({readonly:true,disabled:true});
                        $("#create_date").attr({readonly:true,disabled:true});
                        $("#update_user").attr({readonly:true,disabled:true});
                        $("#update_date").attr({readonly:true,disabled:true});
                        $('#edit_dialog').modal('show');
                    }
                });
            },
            deleteClick:function(id){
                var ids = [];
                if(!!id == true){
                    ids[0] = id;
                }
                else{
                    var checkboxs = $('#data_table :checked');
                    if(checkboxs.size() > 0){
                        var c = 0;
                        for(i = 0; i < checkboxs.size(); i++){
                            var id = checkboxs.eq(i).val();
                            if(id != ""){
                                ids[c++] = id;
                            }
                        }
                    }
                }
                if(ids.length > 0){
                    admin_tool.confirm('请确认是否删除', function(){
                        $.ajax({
                            type: "GET",
                            url: "<?=Url::toRoute('admin-module/delete')?>",
                            data: {"ids":ids},
                            cache: false,
                            dataType:"json",
                            error: function (xmlHttpRequest, textStatus, errorThrown) {
                                admin_tool.alert('msg_info', '====出错了，' + textStatus, 'warning');
                            },
                            success: function(data){
                                admin_tool.alert('msg_info', '删除成功', 'success');
                                app.refreshPage();
                                $('#data_table :checked').prop('checked',false);
                            }
                        });
                    });
                }
                else{
                    admin_tool.alert('msg_info', '请先选择要删除的数据', 'warning');
                }
            },
            editSaveClick:function(){
                $('#edit-form').submit();
            },
            searchIcoClick:function(event){
                $('#ico_dialog').modal('show');
            },
        },
        computed:{
            orderClass(){
                return function(field){
                    var url = window.location.search;
                    var order = "sorting";
                    url = decodeURI(url);
                    if(url.indexOf(field + " desc") > 0){
                        order = "sorting_desc";
                    }
                    else if(url.indexOf(field + " asc") > 0){
                        order = "sorting_asc";
                    }
                    return order;
                }
            }
        },
        mounted:function(){
            $('#edit-form').bind('submit', function(e) {
                e.preventDefault();
                var id = $("#id").val();
                var action = id == "" ? "<?=Url::toRoute('admin-module/create')?>" : "<?=Url::toRoute('admin-module/update')?>";
                $(this).ajaxSubmit({
                    type: "post",
                    dataType:"json",
                    url: action,
                    success: function(value)
                    {
                        if(value.errno == 0){
                            $('#edit_dialog').modal('hide');
                            admin_tool.alert('msg_info', '添加成功', 'success');
                            app.refreshPage();
                        }
                        else{
                            var json = value.data;
                            for(var key in json){
                                $('#' + key).attr({'data-placement':'bottom', 'data-content':json[key], 'data-toggle':'popover'}).addClass('popover-show').popover('show');
                            }
                        }
                    }
                });
            });
            $('#ico_dialog .bs-glyphicons li').click(function(){
                var span = $(this).find('span');
                var css = span.attr('class');
                app.model.ico = css;
                $('#ico_preview').attr('class', css);
                $('#ico_dialog').modal('hide');
            });
        }
    });
</script>
<?php $this->endBlock(); ?>