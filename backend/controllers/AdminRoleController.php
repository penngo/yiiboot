<?php

namespace backend\controllers;

use backend\models\AdminMenu;
use backend\models\AdminRoleMenu;
use backend\models\AdminRoleUser;
use backend\services\AdminMenuService;
use backend\services\AdminRoleMenuService;
use Yii;
use yii\data\Pagination;
use backend\models\AdminRole;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\LinkPager;
/**
 * AdminRoleController implements the CRUD actions for SystemRole model.
 */
class AdminRoleController extends BaseController
{
	public $layout = "lte_main";

    /**
     * Lists all SystemRole models.
     * @return mixed
     */
    public function actionIndex()
    {

        $model = new AdminRole();
        list($querys,$models, $pages, $totalCount, $startNum, $endNum) = $this->getList();
        if (Yii::$app->request->isAjax == true) {
            $result = ['models'=>$models, 'pages'=>$pages, 'totalNum'=>$totalCount,
            'startNum'=>$startNum,'endNum'=>$endNum];
            return $this->asJson(['errno'=>0, 'data'=>$result]);
        }
        else{
            return $this->render('index', [
            'query'=>json_encode($querys),
            'modelLabel'=>json_encode($model->attributeLabels()),
            'models'=>json_encode($models), 'pages'=>$pages, 'totalNum'=>$totalCount,
            'startNum'=>$startNum,
            'endNum'=>$endNum
            ]);
        }
    }


    private function getList(){
        $query = AdminRole::find();
        $querys = Yii::$app->request->get('query');
        if(empty($querys)== false && count($querys) > 0){
            $condition = "";
            $parame = array();
            foreach($querys as $key=>$value){
                $value = trim($value);
                if(empty($value) == false){
                    $parame[":{$key}"]=$value;
                    if(empty($condition) == true){
                        $condition = " {$key}=:{$key} ";
                    }
                    else{
                        $condition = $condition . " AND {$key}=:{$key} ";
                    }
                }
            }
            if(count($parame) > 0){
                $query = $query->where($condition, $parame);
            }
        }
        $totalCount = $query->count();
        $pagination = new Pagination([
            'totalCount' =>$totalCount,
            'pageSize' => '10',
            'pageParam'=>'page',
            'pageSizeParam'=>'per-page']
        );
        $pages = LinkPager::widget([
            'pagination' => $pagination,
            'nextPageLabel' => '»',
            'prevPageLabel' => '«',
            'firstPageLabel' => '首页',
            'lastPageLabel' => '尾页',
        ]);
        $pages = str_replace("\n", '', $pages);
        $orderby = Yii::$app->request->get('orderby', '');
        if(empty($orderby) == false){
            $query = $query->orderBy($orderby);
        }
        $models = $query
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
        $startNum = $pagination->getPage() * $pagination->getPageSize() + 1;
        $endNum = ($pagination->getPage() + 1) * $pagination->getPageSize();
        $endNum = $endNum < $totalCount ?  $endNum : $totalCount;
        return [$querys,$models, $pages, $totalCount,$startNum,$endNum];
    }


    /**
     * Displays a single SystemRole model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $data = $model->getAttributes();
        
        
        return $this->asJson($data);

    }

    /**
     * Creates a new SystemRole model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AdminRole();
        if ($model->load(Yii::$app->request->post())) {
        
            $model->create_user = Yii::$app->user->identity->uname;
            $model->create_date = date('Y-m-d H:i:s');
            $model->update_user = Yii::$app->user->identity->uname;
            $model->update_date = date('Y-m-d H:i:s');        
            if($model->validate() == true && $model->save()){
                $msg = array('errno'=>0, 'msg'=>'保存成功');
                return $this->asJson($msg);
            }
            else{
                $msg = array('errno'=>2, 'data'=>$model->getErrors());
                return $this->asJson($msg);
            }
        } else {
            $msg = array('errno'=>2, 'msg'=>'数据出错');
            return $this->asJson($msg);
        }
    }

    /**
     * Updates an existing SystemRole model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
        
              $model->update_user = Yii::$app->user->identity->uname;
              $model->update_date = date('Y-m-d H:i:s');            if($model->validate() == true && $model->save()){
                $msg = array('errno'=>0, 'msg'=>'保存成功');
                return $this->asJson($msg);
            }
            else{
                $msg = array('errno'=>2, 'data'=>$model->getErrors());
                return $this->asJson($msg);
            }
        } else {
            $msg = array('errno'=>2, 'msg'=>'数据出错');
            return $this->asJson($msg);
        }
    
    }

    /**
     * Deletes an existing SystemRole model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $ids)
    {
        if(count($ids) > 0){
            $c = AdminRole::deleteAll(['in', 'id', $ids]);
            AdminRoleMenu::deleteAll(['in', 'role_id', $ids]);
            AdminRoleUser::deleteAll(['in', 'role_id', $ids]);
            return $this->asJson(array('errno'=>0, 'data'=>$c, 'msg'=>json_encode($ids)));
        }
        else{
            return $this->asJson(array('errno'=>2, 'msg'=>''));
        }
    }

	
	 

    /**
     * Finds the SystemRole model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdminRole the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdminRole::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     *  新的用户权限
     * @param $roleId
     * @return \yii\web\Response
     */
    public function actionGetAllRights($roleId){
        // 当前选择的角色权限
        $roleRights = AdminRoleMenu::findAll(['role_id'=>$roleId]);
        $roleRightsData = [];
        foreach($roleRights as $r){
            $roleRightsData[strval($r->menu_id)] = $r->menu_id;
        }
//        exit(json_encode($roleRightsData));
        $rights = AdminMenu::find()->orderBy(" pid asc ")->asArray()->all();
        $datas = array();
        foreach($rights as $r){
            $id = strval($r['id']);
            $name = $r['name'];
            $type = $r['type'];
            $path = explode('_', $r['path']);;
            if($type === AdminMenuService::$TYPE_MODULE){
                $module = ['mid'=>$id, 'text'=>$name, 'type'=>'m', 'selectable'=>false,'state'=>['checked'=>true],'funs'=>[]];
                $datas[$id] = $module;
            }
            else if($type === AdminMenuService::$TYPE_MENU){
                $module_id = $path[1];
                $menu = ['fid'=>$id, 'text'=>$name, 'type'=>'f', 'selectable'=>false,'state'=>['checked'=>true],'rights'=>[]];
                $datas[$module_id]['funs'][$id] = $menu;
            }
            else if($type === AdminMenuService::$TYPE_RIGHT){
                $module_id = $path[1];
                $right_id = $path[2];
                $right = ['rid'=>$id,'text'=>$name, 'type'=>'r', 'selectable'=>false,'state'=>['checked'=>false]];
                if(isset($roleRightsData[$id]) == true){
                    $right['state']['checked'] = true;
                }
                $datas[$module_id]['funs'][$right_id]['rights'][$id] = $right;
            }
        }
        foreach($datas as $k=>$modules){
            $funs = $modules['funs'];
            foreach($funs as $f=>$fun){
                $rights = $funs[$f]['rights'];
                unset($funs[$f]['rights']);
                $rights = array_values($rights);
                $funs[$f]['nodes'] = $rights;
                // 检查当前功能下所有权限是否选中,
                foreach($rights as $r=>$right){
                    if($right['state']['checked'] == false){
                        $funs[$f]['state']['checked'] = false;
                        break;
                    }
                }
                // 判断当前模块下所有功能是否全选中
                if($datas[$k]['state']['checked'] == true && $funs[$f]['state']['checked'] == false){
                    $datas[$k]['state']['checked'] = false;
                }
            }
            unset($datas[$k]['funs']);
            $funs = array_values($funs);
            $datas[$k]['nodes']=$funs;
        }
        $datas = array_values($datas);
        return $this->asJson($datas);
    }
    
    public function actionSaveRights(array $rids, $roleId){
         
        if(count($rids) > 0){
            $systemRoleMenuService = new AdminRoleMenuService();
            $count = $systemRoleMenuService->saveRights($rids, $roleId, Yii::$app->user->identity->uname);
            if($count > 0){
                return $this->asJson(array('errno'=>0, 'data'=>$count, 'msg'=>'保存成功'));
                return;
            }
        }
        return $this->asJson(array('errno'=>2, 'data'=>'', 'msg'=>'保存失败'));
    }

}
