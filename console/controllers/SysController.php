<?php

namespace console\controllers;
use backend\models\AdminMenu;
use backend\models\AdminModule;
use backend\models\AdminRight;
use backend\models\AdminRightUrl;
use backend\models\AdminRole;
use backend\models\AdminUser;

use backend\models\AdminMenu;
use backend\models\AdminRole;
use backend\services\AdminMenuService;
use backend\models\AdminUser;

use yii\console\Controller;

class SysController extends Controller
{
    /**
     * php71 yii sys/user
     */
    public function actionUser()
    {
        $rows = AdminUser::find()->all();
        foreach ($rows as $row){
            $user = new AdminUser();
            $user->name = $row->uname;
            $user->password = $row->password;
            $user->real_name = '';
            $user->auth_key = $row->auth_key;
            $user->last_ip = $row->last_ip;
            $user->last_time = $row->last_login;
            $user->is_online = $row->is_online;
            $user->status = $row->status;
            $user->des = '';
            $user->create_user = $row->create_user;
            $user->create_date = date('Y-m-d H:i:s');
            $user->update_user = $row->update_user;
            $user->update_date = $row->update_date;
            if($user->save() == false){
                echo json_encode($user->getErrors()) . "\n";
            }

        }
    }
    /**
     * php71 yii sys/menu
     */
    public function actionMenu()
    {
        $modules = AdminModule::find()->all();
        foreach($modules as $module){   // 一级菜单管理
            $module_id = $module->id;
            $display_label = $module->display_label;
            $display_order = $module->display_order;
            $systemMenu1 = new AdminMenu();
            $systemMenu1->type = AdminMenuService::$TYPE_Module;
            $systemMenu1->pid = 0;
            $systemMenu1->path = '';
            $systemMenu1->name = $display_label;
            $systemMenu1->entry_url = '';
            $systemMenu1->weight = $display_order;
            $systemMenu1->create_user = 'admin';
            $systemMenu1->create_date = date('Y-m-d H:i:s');
            $systemMenu1->update_user = 'admin';
            $systemMenu1->update_date = date('Y-m-d H:i:s');
            $systemMenu1->save();

            $adminMenus = AdminMenu::find()->where(['module_id'=>$module_id])->all();
            foreach($adminMenus as $adminMenu){   // 二级菜单管理-用户管理
                $menu_id = $adminMenu->id;
                $menu_name = $adminMenu->menu_name;
                $menu_entry_url = $adminMenu->entry_url;
                $menu_display_order = $adminMenu->display_order;

//                $menu_action = $adminMenu->action;
//                $menu_controller = $adminMenu->controller;
                $systemMenu2 = new AdminMenu();
                $systemMenu2->type = AdminMenuService::$TYPE_MENU;
                $systemMenu2->pid = $systemMenu1->id;
                $systemMenu2->path = '_'.$systemMenu1->id.'_';
                $systemMenu2->name = $menu_name;
                $systemMenu2->entry_url = $menu_entry_url;
                $systemMenu2->weight = $menu_display_order;
                $systemMenu2->create_user = 'admin';
                $systemMenu2->create_date = date('Y-m-d H:i:s');
                $systemMenu2->update_user = 'admin';
                $systemMenu2->update_date = date('Y-m-d H:i:s');
                $systemMenu2->save();

                $adminRights = AdminRight::find()->where(['menu_id'=>$menu_id])->all();
                foreach($adminRights as $adminRight){ // 二级菜单管理-用户管理-添加用户
                    $right_id = $adminRight->id;
                    $systemMenu3 = new AdminMenu();
                    $systemMenu3->type = AdminMenuService::$TYPE_RIGHT;
                    $systemMenu3->pid = $systemMenu2->id;
                    $systemMenu3->path = $systemMenu2->path . $systemMenu2->id . '_';
                    $systemMenu3->name = $adminRight->right_name;
                    $systemMenu3->entry_url = '';
                    $systemMenu3->weight = $adminRight->display_order;
                    $systemMenu3->create_user = 'admin';
                    $systemMenu3->create_date = date('Y-m-d H:i:s');
                    $systemMenu3->update_user = 'admin';
                    $systemMenu3->update_date = date('Y-m-d H:i:s');
                    $systemMenu3->save();

                    // 二级菜单管理-用户管理-添加用户权限->admin-role/index
                    $adminRightUrls = AdminRightUrl::find()->where(['right_id'=>$right_id])->all();
                    echo "===right_id:$right_id,count:".count($adminRightUrls)."\n";
                    foreach($adminRightUrls as $adminRightUrl){
                        $systemMenu4 = new AdminMenu();
                        $systemMenu4->type = AdminMenuService::$TYPE_RIGHT_URL;
                        $systemMenu4->pid = $systemMenu3->id;
                        $systemMenu4->path = $systemMenu3->path . $systemMenu3->id . '_';
                        $systemMenu4->name = $adminRightUrl->url;
                        $systemMenu4->entry_url = $adminRightUrl->url;
                        $systemMenu4->weight = 0;
                        $systemMenu4->create_user = 'admin';
                        $systemMenu4->create_date = date('Y-m-d H:i:s');
                        $systemMenu4->update_user = 'admin';
                        $systemMenu4->update_date = date('Y-m-d H:i:s');
                        if($systemMenu4->save() == false){
                            echo "error=====".json_encode($systemMenu4->getErrors());
                        }
                    }
                }
            }

        }
    }


//    public function actionRole(){
//        $roles = AdminRole::findAll();
//        foreach($roles as $role){
//            $system_role = new SystemRole();
//            $system_role->
//        }
//    }
}