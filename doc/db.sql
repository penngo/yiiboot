/*
SQLyog Trial v12.18 (64 bit)
MySQL - 5.7.14-log : Database - yiiboot
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`yiiboot` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `yiiboot`;

/*Table structure for table `admin_log` */

DROP TABLE IF EXISTS `admin_log`;

CREATE TABLE `admin_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `controller_id` varchar(20) DEFAULT NULL COMMENT '控制器ID',
  `action_id` varchar(20) DEFAULT NULL COMMENT '方法ID',
  `url` varchar(200) DEFAULT NULL COMMENT '访问地址',
  `module_name` varchar(50) DEFAULT NULL COMMENT '模块',
  `func_name` varchar(50) DEFAULT NULL COMMENT '功能',
  `right_name` varchar(50) DEFAULT NULL COMMENT '方法',
  `client_ip` varchar(15) DEFAULT NULL COMMENT '客户端IP',
  `create_user` varchar(50) DEFAULT NULL COMMENT '用户',
  `create_date` datetime DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `index_create_date` (`create_date`),
  KEY `index_create_index` (`create_user`),
  KEY `index_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

/*Data for the table `admin_log` */

insert  into `admin_log`(`id`,`controller_id`,`action_id`,`url`,`module_name`,`func_name`,`right_name`,`client_ip`,`create_user`,`create_date`) values 
(1,'admin-user','index','admin-user/index','','','用户操作','127.0.0.1','admin','2022-04-11 22:31:35'),
(2,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-11 22:31:37'),
(3,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 22:31:38'),
(4,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 22:31:46'),
(5,'admin-menu','index','admin-menu/index','','','二级菜单查看','127.0.0.1','admin','2022-04-11 22:31:48'),
(6,'admin-right','index','admin-right/index','','','路由查看','127.0.0.1','admin','2022-04-11 22:31:50'),
(7,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-11 22:36:28'),
(8,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 22:36:29'),
(9,'admin-menu','index','admin-menu/index','','','二级菜单查看','127.0.0.1','admin','2022-04-11 22:36:31'),
(10,'admin-right','index','admin-right/index','','','路由查看','127.0.0.1','admin','2022-04-11 22:36:55'),
(11,'admin-right','index','admin-right/index','','','路由查看','127.0.0.1','admin','2022-04-11 22:40:14'),
(12,'admin-user','index','admin-user/index','','','用户操作','127.0.0.1','admin','2022-04-11 22:40:16'),
(13,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-11 22:40:16'),
(14,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 22:40:17'),
(15,'admin-menu','index','admin-menu/index','','','二级菜单查看','127.0.0.1','admin','2022-04-11 22:40:20'),
(16,'admin-right','index','admin-right/index','','','路由查看','127.0.0.1','admin','2022-04-11 22:40:22'),
(17,'admin-user','index','admin-user/index','','','用户操作','127.0.0.1','admin','2022-04-11 22:41:31'),
(18,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-11 22:41:31'),
(19,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 22:41:32'),
(20,'admin-menu','index','admin-menu/index','','','二级菜单查看','127.0.0.1','admin','2022-04-11 22:41:34'),
(21,'admin-right','index','admin-right/index','','','路由查看','127.0.0.1','admin','2022-04-11 22:41:35'),
(22,'admin-log','index','admin-log/index',NULL,NULL,NULL,'127.0.0.1','admin','2022-04-11 22:43:04'),
(23,'test-user','index','test-user/index',NULL,NULL,NULL,'127.0.0.1','admin','2022-04-11 22:43:06'),
(24,'admin-log','index','admin-log/index',NULL,NULL,NULL,'127.0.0.1','admin','2022-04-11 22:43:08'),
(25,'test-user','index','test-user/index',NULL,NULL,NULL,'127.0.0.1','admin','2022-04-11 22:43:11'),
(26,'admin-user','index','admin-user/index','','','用户操作','127.0.0.1','admin','2022-04-11 22:43:13'),
(27,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-11 22:43:15'),
(28,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 22:43:16'),
(29,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-11 22:43:18'),
(30,'test-user','index','test-user/index',NULL,NULL,NULL,'127.0.0.1','admin','2022-04-11 22:43:21'),
(31,'admin-log','index','admin-log/index',NULL,NULL,NULL,'127.0.0.1','admin','2022-04-11 22:43:23'),
(32,'test-user','index','test-user/index',NULL,NULL,NULL,'127.0.0.1','admin','2022-04-11 22:43:27'),
(33,'admin-log','index','admin-log/index',NULL,NULL,NULL,'127.0.0.1','admin','2022-04-11 22:43:30'),
(34,'test-user','index','test-user/index',NULL,NULL,NULL,'127.0.0.1','admin','2022-04-11 22:43:32'),
(35,'admin-user','index','admin-user/index','','','用户操作','127.0.0.1','admin','2022-04-11 22:45:05'),
(36,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-11 22:45:06'),
(37,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 22:45:06'),
(38,'test-user','index','test-user/index',NULL,NULL,NULL,'127.0.0.1','admin','2022-04-11 22:48:09'),
(39,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-11 22:48:19'),
(40,'admin-role','get-all-rights','admin-role/get-all-rights','','','分配权限','127.0.0.1','admin','2022-04-11 22:48:21'),
(41,'admin-role','get-all-rights','admin-role/get-all-rights','','','分配权限','127.0.0.1','admin','2022-04-11 22:48:25'),
(42,'admin-role','save-rights','admin-role/save-rights','','','分配权限','127.0.0.1','admin','2022-04-11 22:48:30'),
(43,'admin-log','index','admin-log/index','','','操作','127.0.0.1','admin','2022-04-11 22:48:45'),
(44,'test-user','index','test-user/index','','','test','127.0.0.1','admin','2022-04-11 22:48:46'),
(45,'test-user','index','test-user/index','','','test','127.0.0.1','admin','2022-04-11 23:06:40'),
(46,'admin-log','index','admin-log/index','','','操作','127.0.0.1','admin','2022-04-11 23:06:41'),
(47,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 23:06:44'),
(48,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 23:12:20'),
(49,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 23:12:24'),
(50,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 23:12:44'),
(51,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 23:16:54'),
(52,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-11 23:17:24'),
(53,'admin-user','index','admin-user/index','','','用户操作','127.0.0.1','admin','2022-04-19 21:57:04'),
(54,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-19 21:57:05'),
(55,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-19 21:57:06'),
(56,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-19 21:57:07'),
(57,'admin-user','index','admin-user/index','','','用户操作','127.0.0.1','admin','2022-04-19 21:57:08'),
(58,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-19 21:57:09'),
(59,'test-user','index','test-user/index','','','test','127.0.0.1','admin','2022-04-19 21:57:10'),
(60,'admin-log','index','admin-log/index','','','操作','127.0.0.1','admin','2022-04-19 21:57:11'),
(61,'test-user','index','test-user/index','','','test','127.0.0.1','admin','2022-04-19 21:57:11'),
(62,'test-user','index','test-user/index','','','test','127.0.0.1','admin','2022-04-19 22:11:12'),
(63,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-19 22:11:14'),
(64,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-19 22:18:54'),
(65,'admin-user','index','admin-user/index','','','用户操作','127.0.0.1','admin','2022-04-19 22:18:57'),
(66,'admin-module','index','admin-module/index','','','一级菜单查看','127.0.0.1','admin','2022-04-19 22:18:57'),
(67,'admin-role','index','admin-role/index','','','分配权限','127.0.0.1','admin','2022-04-19 22:18:59'),
(68,'test-user','index','test-user/index','','','test','127.0.0.1','admin','2022-04-19 22:19:00'),
(69,'admin-log','index','admin-log/index','','','操作','127.0.0.1','admin','2022-04-19 22:19:01');

/*Table structure for table `admin_menu` */

DROP TABLE IF EXISTS `admin_menu`;

CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ico` varchar(50) DEFAULT NULL COMMENT '图标',
  `type` varchar(10) DEFAULT NULL COMMENT '类型',
  `pid` int(11) DEFAULT NULL COMMENT '父点',
  `path` varchar(100) DEFAULT NULL COMMENT '父节点路径',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `entry_url` varchar(255) DEFAULT NULL COMMENT '路由地址',
  `weight` int(11) DEFAULT NULL COMMENT '权重',
  `create_user` varchar(100) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(100) DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `index_type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

/*Data for the table `admin_menu` */

insert  into `admin_menu`(`id`,`ico`,`type`,`pid`,`path`,`name`,`entry_url`,`weight`,`create_user`,`create_date`,`update_user`,`update_date`) values 
(1,'glyphicon glyphicon-list','module',0,'','权限管理','',10,'admin','2021-08-12 11:22:07','admin','2022-04-09 16:33:59'),
(2,NULL,'menu',1,'_1_','菜单管理','admin-module/index',1,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(3,'','right',2,'_1_2_','一级菜单查看','',1,'admin','2021-08-12 11:22:07','admin','2022-04-09 16:33:50'),
(6,NULL,'right',2,'_1_2_','一级菜单添加','',2,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(7,NULL,'right_url',6,'_1_2_6_','admin-module/create','admin-module/create',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(8,NULL,'right_url',6,'_1_2_6_','admin-module/update','admin-module/update',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(9,NULL,'right',2,'_1_2_','一级菜单删除','',3,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(10,NULL,'right_url',9,'_1_2_9_','admin-module/delete','admin-module/delete',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(11,NULL,'right',2,'_1_2_','二级菜单查看','',4,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(12,NULL,'right_url',11,'_1_2_11_','admin-menu/index','admin-menu/index',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(13,NULL,'right_url',11,'_1_2_11_','admin-menu/view','admin-menu/view',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(14,NULL,'right',2,'_1_2_','二级菜单添加','',5,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(15,NULL,'right_url',14,'_1_2_14_','admin-menu/create','admin-menu/create',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(16,NULL,'right_url',14,'_1_2_14_','admin-menu/update','admin-menu/update',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(17,NULL,'right',2,'_1_2_','二级菜单删除','',6,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(18,NULL,'right_url',17,'_1_2_17_','admin-menu/delete','admin-menu/delete',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(19,NULL,'right',2,'_1_2_','路由查看','',7,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(20,NULL,'right_url',19,'_1_2_19_','admin-right/index','admin-right/index',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(21,NULL,'right_url',19,'_1_2_19_','admin-right/view','admin-right/view',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(22,NULL,'right_url',19,'_1_2_19_','admin-right/right-action','admin-right/right-action',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(23,NULL,'right',2,'_1_2_','路由添加','',8,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(24,NULL,'right_url',23,'_1_2_23_','admin-right/create','admin-right/create',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(25,NULL,'right_url',23,'_1_2_23_','admin-right/update','admin-right/update',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(26,NULL,'right',2,'_1_2_','路由删除','',9,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(27,NULL,'right_url',26,'_1_2_26_','admin-right/delete','admin-right/delete',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(28,NULL,'menu',1,'_1_','角色管理','admin-role/index',2,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(29,NULL,'right',28,'_1_28_','角色操作','',1,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(30,NULL,'right_url',29,'_1_28_29_','admin-role/index','admin-role/index',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(31,NULL,'right_url',29,'_1_28_29_','admin-role/view','admin-role/view',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(32,NULL,'right_url',29,'_1_28_29_','admin-role/create','admin-role/create',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(33,NULL,'right_url',29,'_1_28_29_','admin-role/update','admin-role/update',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(34,NULL,'right_url',29,'_1_28_29_','admin-role/delete','admin-role/delete',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(35,NULL,'right_url',29,'_1_28_29_','admin-role/get-all-rights','admin-role/get-all-rights',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(36,NULL,'right_url',29,'_1_28_29_','admin-role/save-rights','admin-role/save-rights',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(37,NULL,'right',28,'_1_28_','分配用户','',2,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(38,NULL,'right_url',37,'_1_28_37_','admin-user-role/index','admin-user-role/index',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(39,NULL,'right_url',37,'_1_28_37_','admin-user-role/view','admin-user-role/view',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(40,NULL,'right_url',37,'_1_28_37_','admin-user-role/create','admin-user-role/create',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(41,NULL,'right_url',37,'_1_28_37_','admin-user-role/update','admin-user-role/update',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(42,NULL,'right_url',37,'_1_28_37_','admin-user-role/delete','admin-user-role/delete',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(43,NULL,'right',28,'_1_28_','分配权限','',3,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(44,NULL,'right_url',43,'_1_28_43_','admin-role/index','admin-role/index',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(45,NULL,'right_url',43,'_1_28_43_','admin-role/view','admin-role/view',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(46,NULL,'right_url',43,'_1_28_43_','admin-role/create','admin-role/create',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(47,NULL,'right_url',43,'_1_28_43_','admin-role/update','admin-role/update',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(48,NULL,'right_url',43,'_1_28_43_','admin-role/delete','admin-role/delete',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(49,NULL,'right_url',43,'_1_28_43_','admin-role/get-all-rights','admin-role/get-all-rights',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(50,NULL,'right_url',43,'_1_28_43_','admin-role/save-rights','admin-role/save-rights',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(51,NULL,'menu',1,'_1_','用户管理','admin-user/index',3,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(52,NULL,'right',51,'_1_51_','用户操作','',1,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(53,NULL,'right_url',52,'_1_51_52_','admin-user/index','admin-user/index',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(54,NULL,'right_url',52,'_1_51_52_','admin-user/view','admin-user/view',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(55,NULL,'right_url',52,'_1_51_52_','admin-user/create','admin-user/create',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(56,NULL,'right_url',52,'_1_51_52_','admin-user/update','admin-user/update',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(57,NULL,'right_url',52,'_1_51_52_','admin-user/delete','admin-user/delete',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(58,'glyphicon glyphicon-list','module',0,'','日志管理','',1,'admin','2021-08-12 11:22:07','admin','2021-08-25 15:15:11'),
(59,NULL,'menu',58,'_58_','操作日志','admin-log/index',1,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(60,NULL,'right',59,'_58_59_','操作','',1,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(61,NULL,'right_url',60,'_58_59_60_','admin-log/index','admin-log/index',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(62,NULL,'right_url',60,'_58_59_60_','admin-log/view','admin-log/view',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(63,NULL,'right_url',60,'_58_59_60_','admin-log/create','admin-log/create',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(64,NULL,'right_url',60,'_58_59_60_','admin-log/update','admin-log/update',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(65,NULL,'right_url',60,'_58_59_60_','admin-log/delete','admin-log/delete',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(66,NULL,'menu',58,'_58_','测试生成','test-user/index',0,'admin','2021-08-12 11:22:07','admin','2021-08-12 11:22:07'),
(67,'','right',66,'_58_66_','test','',0,'admin','2021-08-12 11:22:07','admin','2021-08-24 15:06:21'),
(75,'',NULL,NULL,NULL,'查看',NULL,0,'admin','2021-08-23 16:54:12','admin','2021-08-23 16:54:12'),
(87,NULL,'right_url',67,'_58_66_67_','test-user/index','test-user/index',0,'admin','2021-08-24 15:06:21','admin','2021-08-24 15:06:21'),
(88,NULL,'right_url',67,'_58_66_67_','test-user/view','test-user/view',0,'admin','2021-08-24 15:06:21','admin','2021-08-24 15:06:21'),
(89,NULL,'right_url',3,'_1_2_3_','admin-module/index','admin-module/index',0,'admin','2022-04-09 16:33:50','admin','2022-04-09 16:33:50'),
(90,NULL,'right_url',3,'_1_2_3_','admin-module/view','admin-module/view',0,'admin','2022-04-09 16:33:50','admin','2022-04-09 16:33:50');

/*Table structure for table `admin_message` */

DROP TABLE IF EXISTS `admin_message`;

CREATE TABLE `admin_message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `msg` varchar(1000) DEFAULT NULL COMMENT '留言内容',
  `expiry_days` int(5) unsigned DEFAULT NULL COMMENT '有效天数',
  `create_user` varchar(50) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_user` varchar(50) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin_message` */

insert  into `admin_message`(`id`,`msg`,`expiry_days`,`create_user`,`create_date`,`update_user`,`update_date`) values 
(1,'测试文本',1,'admin','2014-11-21 18:47:20','admin','2014-11-21 18:47:27');

/*Table structure for table `admin_role` */

DROP TABLE IF EXISTS `admin_role`;

CREATE TABLE `admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `des` varchar(400) DEFAULT NULL COMMENT '角色描述',
  `create_user` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin_role` */

insert  into `admin_role`(`id`,`name`,`des`,`create_user`,`create_date`,`update_user`,`update_date`) values 
(1,'超级管理员','拥有所有权限','admin','2022-04-06 22:49:29','admin','2022-04-09 16:34:24');

/*Table structure for table `admin_role_menu` */

DROP TABLE IF EXISTS `admin_role_menu`;

CREATE TABLE `admin_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int(11) NOT NULL COMMENT '角色主键',
  `menu_id` int(11) NOT NULL COMMENT '菜单主键',
  `create_user` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(50) DEFAULT NULL COMMENT '修改人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `index_role_id` (`role_id`),
  KEY `index_menu_id` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

/*Data for the table `admin_role_menu` */

insert  into `admin_role_menu`(`id`,`role_id`,`menu_id`,`create_user`,`create_date`,`update_user`,`update_date`) values 
(29,1,3,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(30,1,6,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(31,1,9,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(32,1,11,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(33,1,14,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(34,1,17,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(35,1,19,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(36,1,23,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(37,1,26,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(38,1,29,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(39,1,37,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(40,1,43,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(41,1,52,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(42,1,60,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30'),
(43,1,67,'admin','2022-04-11 22:48:30','admin','2022-04-11 22:48:30');

/*Table structure for table `admin_role_user` */

DROP TABLE IF EXISTS `admin_role_user`;

CREATE TABLE `admin_role_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色',
  `create_user` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(50) DEFAULT NULL COMMENT '修改人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `index_user_id` (`user_id`),
  KEY `index_role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin_role_user` */

insert  into `admin_role_user`(`id`,`user_id`,`role_id`,`create_user`,`create_date`,`update_user`,`update_date`) values 
(1,156,1,'admin','2022-04-06 22:49:40','admin','2022-04-06 22:49:40');

/*Table structure for table `admin_user` */

DROP TABLE IF EXISTS `admin_user`;

CREATE TABLE `admin_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uname` varchar(100) NOT NULL COMMENT '用户名',
  `real_name` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `password` varchar(200) NOT NULL COMMENT '密码',
  `auth_key` varchar(50) DEFAULT NULL COMMENT '自动登录key',
  `last_ip` varchar(50) DEFAULT NULL COMMENT '最近一次登录ip',
  `last_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `is_online` char(1) DEFAULT 'n' COMMENT '是否在线',
  `status` int(11) NOT NULL DEFAULT '10' COMMENT '状态',
  `des` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_user` varchar(100) DEFAULT NULL COMMENT '创建人',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(101) DEFAULT NULL COMMENT '更新人',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;

/*Data for the table `admin_user` */

insert  into `admin_user`(`id`,`uname`,`real_name`,`password`,`auth_key`,`last_ip`,`last_time`,`is_online`,`status`,`des`,`create_user`,`create_date`,`update_user`,`update_date`) values 
(2,'test','','$2y$13$9O6bKJieocg//oSax9fZOOuljAKarBXknqD8.RyYg60FfNjS7SoqK',NULL,'',NULL,'n',10,'','admin','2021-08-06 17:03:29','admin','2019-01-28 10:35:02'),
(156,'admin','','$2y$13$9O6bKJieocg//oSax9fZOOuljAKarBXknqD8.RyYg60FfNjS7SoqK',NULL,'127.0.0.1','2022-04-11 22:48:42','n',10,'','admin','2021-08-06 17:03:29','admin','2014-09-03 12:19:12');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
