package com.penngo.yiiboot.web;

import com.aventstack.extentreports.testng.listener.ExtentIReporterSuiteClassListenerAdapter;
import com.penngo.yiiboot.base.BaseChromeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.time.Duration;

@Listeners(ExtentIReporterSuiteClassListenerAdapter.class)
public class LoginTest  extends BaseChromeTest {

    /**
     * 首页
     */
    @Test(description="登录()")
    public void testLogin(){
        login();
    }
}
