package com.penngo.yiiboot.web;

import com.aventstack.extentreports.testng.listener.ExtentIReporterSuiteClassListenerAdapter;
import com.penngo.yiiboot.base.BaseChromeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.List;

/**
 * 用户管理菜单
 */
@Listeners(ExtentIReporterSuiteClassListenerAdapter.class)
public class UserTest extends BaseChromeTest {

    /**
     * 首页
     */
    @Test(description="登录()")
    public void testLogin(){
        login();
    }

    /**
     * 首页
     */
    @Test(description="点击用户菜单")
    public void testUserMenu(){
        // 点击权限管理
        List<WebElement> menuA = driver.findElements(By.cssSelector(".sidebar-menu li a"));
        for (WebElement a:menuA){
            if(a.getText().indexOf("权限管理") > -1){
                a.click();
                System.out.println("找到权限管理菜单=======");
                break;
            }
        }

        // 点击用户管理
        for (WebElement a:menuA){
            if(a.getText().indexOf("用户管理") > -1){
                a.click();
                System.out.println("找到用户管理菜单=======");
                break;
            }
        }

        
    }
}
