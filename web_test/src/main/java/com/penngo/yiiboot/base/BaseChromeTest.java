package com.penngo.yiiboot.base;

import com.penngo.yiiboot.utils.Tool;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * ChromeDriver
 */
@Slf4j
public abstract class BaseChromeTest {
    public final String url = "http://localhost:81/yiiboot_master/backend/web/index.php";
    public WebDriver driver = null;

    @BeforeClass
    public void setUpAll(){
        System.out.println("setUpAll===============");
        log.info("setUpAll===============");
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<>();
//        prefs.put("profile.default_content_setting_values.images", 2);
//        prefs.put("profile.default_content_setting_values.plugins", 2);
//        prefs.put("profile.default_content_setting_values.geolocation", 2);
//        prefs.put("profile.default_content_setting_values.notifications", 2);
        options.setExperimentalOption("prefs", prefs);
        driver = DriverBuilder.createChrome(false, true, options);
    }


    public void login(){
        System.out.println("testLogin===============");
        driver.get(url + "?r=site/index");
        driver.findElement(By.cssSelector("#username")).sendKeys("admin");
        driver.findElement(By.cssSelector("#password")).sendKeys("123456");
        driver.findElement(By.cssSelector("#login_btn")).click();

        boolean b = new WebDriverWait(driver, Duration.ofSeconds(3))
                .until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.cssSelector(".box-title")), "系统信息"));
        Assert.assertTrue(b, "登录成功");

    }

    @AfterClass
    public void tearDownAll(){
        System.out.println("tearDownAll===============");
        Tool.sleep(30);
        if(driver != null){
            driver.quit();
        }
    }


    public void sleep(int second){
        try{
            Thread.sleep(second * 1000);
        }
        catch(Exception e){

        }
    }
}
