package com.penngo.yiiboot.base;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class DriverBuilder {

    static {
        // 关闭控制selenium命令行日志输出
//        System.setProperty("webdriver.chrome.silentOutput", "true");
//        java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.WARNING);
        String os = System.getProperty("os.name").toLowerCase();
        // linux上运行
        if(os.indexOf("windows") == -1){
            System.setProperty("webdriver.chrome.driver", "webdriver/chromedriver");
        }
        // window上运行，本机调试
        else{
            System.setProperty("webdriver.chrome.driver", "webdriver/chromedriver.exe");

        }
    }

    public static ChromeDriver createChrome(){
        return createChrome(null);
    }

    public static ChromeDriver createChrome(ChromeOptions options){
        return createChrome(false, false, null);
    }
    public static ChromeDriver createChrome(boolean headless, boolean isImplicitlyWait){
        return createChrome(headless, isImplicitlyWait, null);
    }
    public static ChromeDriver createChrome(boolean isImplicitlyWait){
        return createChrome(false, isImplicitlyWait, null);
    }
    public static ChromeDriver createChrome(boolean headless, boolean isImplicitlyWait, ChromeOptions options){
        if(options == null){
            options = new ChromeOptions();
        }
        options.setHeadless(headless);
        options.setAcceptInsecureCerts(true);
        options.addArguments("window-size=1920x1080"); //分辨率

        ChromeDriver driver = new ChromeDriver(options);

        if(isImplicitlyWait == true){
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
            driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
        }
        return driver;
    }
    /**
     * 浏览器缓存路径
     * @return
     */
    protected static String getTempPath(){
        String path =  System.getProperty("user.dir") +  "/logs/Chrome/";
        return path;
    }
}
