<?php
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

$modelClass = $generator->modelClass;
$mn = explode('\\', $modelClass);
$modelName = array_pop($mn);
$model = new $modelClass();
if(method_exists($model, 'getTableColumnInfo') == false){
    exit('error, ' . get_class($model) . ' no getTableColumnInfo method. can not use common (..\..\vendor\yiisoft\yii2-gii\generators\crud\common) to generated.');
}
$tableColumnInfo = $model->getTableColumnInfo();
$controllerClass = $generator->controllerClass;
$controllerName = substr($controllerClass, 0, strlen($controllerClass) - 10);

?>

<?="<?php\n"?>
use yii\bootstrap5\ActiveForm;
use yii\helpers\Url;

?>

<?="<?php \$this->beginBlock('header');  ?>\n"?>
<!-- <head></head>中代码块 -->
<?="<?php \$this->endBlock(); ?>"?>


<!-- Main content -->
<section class="content">
    <div id="msg_info"></div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">数据列表</h3>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <button id="create_btn" @click="createClick()" type="button" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i>添&nbsp;&emsp;加</button>
<!--                            |-->
                            <button id="delete_btn" @click="deleteClick()" type="button" class="btn btn-xs btn-danger">批量删除</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div id="data_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <!-- row start search-->
                        <div class="row">
                            <div class="col-sm-12">
                                <?="<?php ActiveForm::begin(['id' => 'search-form', 'method'=>'get', 'options' => ['class' => 'form-inline'], 'action'=>Url::toRoute('".Inflector::camel2id(StringHelper::basename($controllerName))."/index')]); ?>"?>
                                <?php foreach($tableColumnInfo as $key=>$column){
                                    if($column['isSearch'] === true){
                                        echo "\n";
                                        echo "                    <div class=\"form-group\" style=\"margin: 5px;\">\n";
                                        echo "                        <label>{{modelLabel.$key}}:</label>\n";
                                        echo "                        <input type=\"text\" class=\"form-control\" id=\"query[$key]\" name=\"query[$key]\"  :value=\"query['$key']\">\n";
                                        echo "                    </div>\n";
                                    }
                                }

                                ?>
                                <div class="form-group">
                                    <a @click="searchClick" class="btn btn-primary btn-sm" href="#"> <i class="glyphicon glyphicon-zoom-in icon-white"></i>搜索</a>
                                </div>
                                <?="<?php ActiveForm::end(); ?>"?>
                            </div>
                        </div>
                        <!-- row end search -->

                        <!-- row start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="data_table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="data_table_info">
                                    <thead class="text-nowrap">
                                    <tr role="row">
                                        <th><input id="data_table_check" type="checkbox"></th>
<?php
foreach($tableColumnInfo as $key=>$column){
    if($column['isDisplay'] == true){
        ?>
                                        <th @click="orderClick('<?=$key?>')" :class="orderClass('<?=$key?>')" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >{{modelLabel.<?=$key?>}}</th>
<?php
    }
}?>
                                        <th tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" aria-sort="ascending" >操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <template v-for="m in models">
                                        <tr :id="'rowid_' + m.id">
                                            <td><label><input type="checkbox" :value="m.id"></label></td>
<?php
    foreach($tableColumnInfo as $key=>$column){
        if($column['isDisplay'] === true){  ?>
                                            <td>{{m.<?=$key?>}}</td>
<?php
        }
        else{  ?>
                                            <td>{{m.<?=$key?>}}</td>
<?php   }
    }
?>
                                            <td class="center">
                                                <a id="edit_btn" @click="editClick(m.id)" class="btn btn-primary btn-sm" href="#" title="修改"> <i class="glyphicon glyphicon-edit icon-white"></i></a>
                                                <a id="delete_btn" @click="deleteClick(m.id)" class="btn btn-danger btn-sm" href="#" title="删除"> <i class="glyphicon glyphicon-trash icon-white"></i></a>
                                            </td>
                                        </tr>
                                    </template>
                                    </tbody>
                                    <!-- <tfoot></tfoot> -->
                                </table>
                            </div>
                        </div>
                        <!-- row end -->

                        <!-- row start -->
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="data_table_info" role="status" aria-live="polite">
                                    <div class="infos">
                                        从 {{startNum}}            		到  {{endNum}}            		 共 {{totalNum}} 条记录</div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="data_table_paginate">
                                    <span v-if="pages != ''" v-html="pages"></span>
                                </div>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<div class="modal fade" id="edit_dialog" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">编辑</h4>
            </div>
            <div class="modal-body">
            <?='<?php $form = ActiveForm::begin(["id" => "edit-form", "class"=>"form-horizontal", "action"=>Url::toRoute("'.Inflector::camel2id(StringHelper::basename($controllerName)).'/save")]); ?>'?>
                <?php foreach($tableColumnInfo as $key=>$column){
                    echo "\n";
                    $isNeed = $column['allowNull'] == false ? '必填' : '';
                    //$widget = '';
                    $widget = '            <div id="'.$key.'_div" class="form-group">'."\n";
                    $widget.= '                <label for="'.$key.'" class="col-sm-2 control-label">{{modelLabel.'.$key.'}}</label>'."\n";
                    $widget.= '                <div class="col-sm-10">'."\n";
                    $widget.= '==widget==';
                    $widget.= '                </div>'."\n";
                    $widget.= '                <div class="clearfix"></div>'."\n";
                    $widget.= '            </div>'."\n";

                    switch($column['inputType']){
                        case 'hidden':
                            if($column['isPrimaryKey'] == true){
                                $inputWidget = '            <input type="hidden" class="form-control" id="'.$key.'" name="'.$key.'" v-model="model.'.$key.'" />'."\n";
                            }
                            else{
                                $inputWidget = '            <input type="hidden" class="form-control" id="'.$key.'" name="'.$modelName.'['.$key.']" v-model="model.'.$key.'" />'."\n";
                            }
                            echo $inputWidget;
                            break;
                        case 'text':
                            $inputWidget = '                    <input type="text" class="form-control" id="'.$key.'" name="'.$modelName.'['.$key.']" placeholder="'.$isNeed . '" v-model="model.'.$key.'" />'."\n";
                            echo str_replace('==widget==', $inputWidget, $widget);
                            break;
                        case 'select':
                            //UdcCode::$YES_NO
                            $inputWidget = '<select class="form-control" name="'.$modelName.'['.$key.']" id="'.$key.'" v-model="model.'.$key.'">';
                            $udc = $column['udc'];
                            $code = str_ireplace('UdcCode::$', '', $udc);
                            $vars = get_class_vars('common\utils\UdcCode');
                            if(empty($vars[$code]) == false){
                                foreach($vars[$code] as $k=>$v){
                                    $inputWidget .= "<option value=\"{$k}\">{$v}</option>";
                                }
                            }
                            $inputWidget .= '</select>';
                            echo str_replace('==widget==', $inputWidget, $widget);
                            break;
                        case 'radio':
                            $udc = $column['udc'];
                            $code = str_ireplace('UdcCode::$', '', $udc);
                            $vars = get_class_vars('common\utils\UdcCode');
                            $inputWidget = '';
                            if(empty($vars[$code]) == false){
                                foreach($vars[$code] as $k=>$v){
                                    $inputWidget .= '<label class="radio-inline">' .
                                        '<input type="radio" name="'.$modelName.'['.$key.']" id="'.$key.'" value="'.$k.'" v-model="model.'.$key.'">' . $v .
                                        '</label>'."\n";
                                }
                            }
                            echo str_replace('==widget==', $inputWidget, $widget);
                            break;
                        case 'checkbox':
                            $udc = $column['udc'];
                            $code = str_ireplace('UdcCode::$', '', $udc);
                            $vars = get_class_vars('common\utils\UdcCode');
                            $inputWidget = '';
                            if(empty($vars[$code]) == false){
                                foreach($vars[$code] as $k=>$v){
                                    $inputWidget .= '<label class="checkbox-inline">' .
                                        '<input type="checkbox" name="'.$modelName.'['.$key.'][]" id="inlineCheckbox1" value="'.$k.'" v-model="model.'.$key.'">'. $v .
                                        '</label>' . "\n";
                                }
                            }
                            echo str_replace('==widget==', $inputWidget, $widget);
                            break;

                        case 'file':
                            $inputWidget = '<input type="hidden" class="form-control" id="'.$key.'" name="'.$modelName.'['.$key.']" v-model="model.'.$key.'" />' .
                                '<input id="'.$key.'_file" type="file" name="'.$key.'_file" class="file" data-overwrite-initial="true" data-min-file-count="1">';
                            echo str_replace('==widget==', $inputWidget, $widget);
                            break;
                        case 'password':
                            $inputWidget = '<input name="password" id="password" type="password" class="form-control" v-model="model.'.$key.' "';
                            echo str_replace('==widget==', $inputWidget, $widget);
                            break;
                    }
                }
                ?>
            <?= "<?php ActiveForm::end(); ?>" ?>

            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">关闭</a>
                <a @click="editSaveClick" id="edit_dialog_ok" href="#" class="btn btn-primary">确定</a>
            </div>
        </div>
    </div>
</div>
<?="<?php \$this->beginBlock('footer');  ?>\n"?>
<!-- <body></body>后代码块 -->
<script>
    var app = new Vue({
        el: '.content-wrapper',  //  .content
        data: {
            modelLabel:<?="<?=\$modelLabel?>"?>,
            model: {},
            models:<?="<?=\$models?>"?>,
            pages:<?="'<?=\$pages?>'"?>,
            startNum:<?="<?=\$startNum?>"?>,
            endNum:<?="<?=\$endNum?>"?>,
            totalNum:<?="<?=\$totalNum?>"?>,
            query:<?="!!<?=\$query?> ? <?=\$query?> : {}"?>,
        },
        methods:{
            refreshPage:function(){
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.models = [];
                        app.models = data.data.models;
                        app.pages = data.data.pages;
                        app.startNum = data.data.startNum;
                        app.endNum = data.data.endNum;
                        app.totalNum = data.data.totalNum;
                    }
                });
            },
            createClick:function(){
                app.model = {};
                $('#edit_dialog').modal('show');
            },
            searchClick:function(){
                $('#search-form').submit();
            },
            orderClick:function(field){
                var url = window.location.search;
                var optemp = field + " desc";
                if(url.indexOf('orderby') != -1){
                    url = url.replace(/orderby=([^&?]*)/ig,  function($0, $1){
                        var optemp = field + " desc";
                        optemp = decodeURI($1) != optemp ? optemp : field + " asc";
                        return "orderby=" + optemp;
                    });
                }
                else{
                    if(url.indexOf('?') != -1){
                        url = url + "&orderby=" + encodeURI(optemp);
                    }
                    else{
                        url = url + "?orderby=" + encodeURI(optemp);
                    }
                }
                window.location.href=url;
            },
            editClick:function(id){
                $.ajax({
                    type: "GET",
                    url: <?="\"<?=Url::toRoute('".Inflector::camel2id(StringHelper::basename($controllerName))."/view')?>\""?>,
                    data: {"id":id},
                    cache: false,
                    dataType:"json",
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("出错了，" + textStatus);
                    },
                    success: function(data){
                        app.model = data;
                        $("#update_date").attr({readonly:false,disabled:false});
                        $('#edit_dialog').modal('show');
                    }
                });
            },
            deleteClick:function(id){
                var ids = [];
                if(!!id == true){
                    ids[0] = id;
                }
                else{
                    var checkboxs = $('#data_table :checked');
                    if(checkboxs.size() > 0){
                        var c = 0;
                        for(i = 0; i < checkboxs.size(); i++){
                            var id = checkboxs.eq(i).val();
                            if(id != ""){
                                ids[c++] = id;
                            }
                        }
                    }
                }
                if(ids.length > 0){
                    admin_tool.confirm('请确认是否删除', function(){
                        $.ajax({
                            type: "GET",
                            url: <?="\"<?=Url::toRoute('".Inflector::camel2id(StringHelper::basename($controllerName))."/delete')?>\""?>,
                            data: {"ids":ids},
                            cache: false,
                            dataType:"json",
                            error: function (xmlHttpRequest, textStatus, errorThrown) {
                                admin_tool.alert('msg_info', '====出错了，' + textStatus, 'warning');
                            },
                            success: function(data){
                                admin_tool.alert('msg_info', '删除成功', 'success');
                                // window.location.reload();
                                app.refreshPage();
                                // this.$forceUpdate();
                                $('#data_table :checked').prop('checked',false);
                            }
                        });
                    });
                }
                else{
                    admin_tool.alert('msg_info', '请先选择要删除的数据', 'warning');
                }
            },
            editSaveClick:function(){
                $('#edit-form').submit();
            }
        },
        computed:{
            orderClass(){
                return function(field){
                    var url = window.location.search;
                    var order = "sorting";
                    url = decodeURI(url);
                    if(url.indexOf(field + " desc") > 0){
                        order = "sorting_desc";
                    }
                    else if(url.indexOf(field + " asc") > 0){
                        order = "sorting_asc";
                    }
                    return order;
                }
            }
        },
        mounted:function(){
            $('#edit-form').bind('submit', function(e) {
                e.preventDefault();
                var id = $("#id").val();
                var action = id == "" ? <?="\"<?=Url::toRoute('".Inflector::camel2id(StringHelper::basename($controllerName))."/create')?>\""?> : <?="\"<?=Url::toRoute('".Inflector::camel2id(StringHelper::basename($controllerName))."/update')?>\""?>;
                $(this).ajaxSubmit({
                    type: "post",
                    dataType:"json",
                    url: action,
                    success: function(value)
                    {
                        if(value.errno == 0){
                            $('#edit_dialog').modal('hide');
                            admin_tool.alert('msg_info', '添加成功', 'success');
                            app.refreshPage();
                        }
                        else{
                            var json = value.data;
                            for(var key in json){
                                $('#' + key).attr({'data-placement':'bottom', 'data-content':json[key], 'data-toggle':'popover'}).addClass('popover-show').popover('show');
                            }
                        }
                    }
                });
            });
        }
    });
</script>
<?="<?php \$this->endBlock(); ?>"?>
